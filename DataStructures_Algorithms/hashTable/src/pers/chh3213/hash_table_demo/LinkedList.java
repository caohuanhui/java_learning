package pers.chh3213.hash_table_demo;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.hash_table_demo
 * @ClassName : LinkedList.java
 * @createTime : 2022/2/16 11:06
 * @Email :
 * @Description :
 */
public class LinkedList {
    private Student head = new Student();

    /**
     * 增
     * @param student
     */
    public void add(Student student){
        if(head.next==null){
            head.next=student;
            return;
        }
        //尾插法
        Student temp = head.next;
        while(temp.next != null) {
            temp = temp.next;
        }
        temp.next = student;
    }

    /**
     * 通过id查找
     * @param id
     */
    public void findId(int id){
        if(head.next==null){
            System.out.println("空链表");
            return;
        }
        Student temp = head;
        while(temp.next != null) {
            temp = temp.next;
            if(temp.id == id) {
                //找到学生，打印学生信息
                System.out.println("该学生信息：" + temp);
                return;
            }
        }
        System.out.println("未找到该学生信息");
    }

    /**
     * 通过id删除
     * @param id
     */
    public void del(int id){
        if(head.next==null){
            System.out.println("空链表");
            return;
        }
        Student temp = head;
        while(temp.next != null) {
            if(temp.next.id == id&&temp.next.next!=null) {
                temp.next = temp.next.next;
            }else if(temp.next.id == id&&temp.next.next==null){
                temp.next = null;
            }
            temp = temp.next;
        }
        System.out.println("未找到该学生信息");
    }

    /**
     * 改
     * @param id
     * @param newID
     * @param newName
     */
    public void modify(int id, int newID,String newName){
        if(head.next==null){
            System.out.println("空链表");
            return;
        }
        Student temp = head;
        while(temp.next != null) {
            temp = temp.next;
            if(temp.id == id) {
                //找到学生，修改学生信息
                temp.name = newName;
                temp.id = newID;
                return;
            }
        }
        System.out.println("未找到该学生信息");
    }
    /**
     * 打印
     */
    public void show(){
        if(head.next == null) {
            System.out.println("链表为空");
            return;
        }
        Student temp = head;
        while(temp.next != null) {
            temp = temp.next;
            System.out.print(temp + " ");
        }
        System.out.println();
    }
}
