package pers.chh3213.hash_table_demo;


/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.hash_table_demo
 * @ClassName : HashTab.java
 * @createTime : 2022/2/16 11:07
 * @Email :
 * @Description :
 */
public class HashTab {
    private LinkedList[] linkedLists;
    private int size;

    public static void main(String[] args) {
        //创建学生
        Student student1 = new Student(1, "Nyima");
        Student student2 = new Student(2, "Lulu");
        Student student6 = new Student(6, "WenWen");
        HashTab hashTab = new HashTab(5);
        hashTab.add(student1);
        hashTab.add(student2);
        hashTab.add(student6);
        hashTab.show();
        //通过id查找学生信息
        hashTab.findId(6);
    }
    public HashTab(int size) {
        this.size = size;
        linkedLists = new LinkedList[size];
        //初始化每个链表，不然会抛出空指针异常
        for(int i=0; i<this.size; i++) {
            //对每个链表进行初始化操作
            linkedLists[i] = new LinkedList();
        }
    }

    /**
     * 获取散列值
     * @param id
     * @return
     */
    private int getHash(int id){
        return id%size;
    }
    public void add(Student student){
        int hashId = getHash(student.id);
        linkedLists[hashId].add(student);
    }
    public void show(){
        for (int i = 0; i < size; i++) {
            linkedLists[i].show();
        }
    }
    public void findId(int id){
        int hashId = getHash(id);
        linkedLists[hashId].findId(id);
    }
}
