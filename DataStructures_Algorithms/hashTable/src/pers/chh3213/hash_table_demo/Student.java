package pers.chh3213.hash_table_demo;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.hash_table_demo
 * @ClassName : Student.java
 * @createTime : 2022/2/16 11:05
 * @Email :
 * @Description :
 */
public class Student {
    int id;
    String name;
    Student next;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Student() {
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
