package pers.chh3213.google;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.google
 * @ClassName : EmpLinkedList.java
 * @createTime : 2022/2/16 11:39
 * @Email :
 * @Description :
 */
public class EmpLinkedList {
    private Emp head;

    /**
     * 添加雇员
     * @param emp
     */
    public void add(Emp emp){
        if(head == null){
            head=emp;
            return;
        }
        //尾插法
        Emp temp = head;
        while (temp.next!=null){
            temp = temp.next;
        }
        temp.next = emp;
    }

    /**
     * 遍历链表信息
     * @param no
     */
    public void list(int no){
        if(head==null){
            System.out.printf("第%d个链表为空",no+1);
            System.out.println();
            return;
        }
        System.out.println();
        System.out.printf("第%d个链表的信息为",no+1);
        Emp temp = head;
        while (temp!=null){
            System.out.println(temp);
            temp = temp.next;
        }
    }
    public Emp findEmpById(int id){
        if(head==null){
            System.out.println("链表为空");
            return null;
        }
        Emp temp = head;
        while (temp!=null){
            if(temp.id==id){
                return temp;
            }
            temp=temp.next;
        }
        System.out.println("无该雇员");
        return null;
    }
}
