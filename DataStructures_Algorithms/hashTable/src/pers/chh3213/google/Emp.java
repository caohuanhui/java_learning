package pers.chh3213.google;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.google
 * @ClassName : Emp.java
 * @createTime : 2022/2/16 11:36
 * @Email :
 * @Description :
 */
public class Emp {
    public int id;
    public String name;
    //next默认为null
    public Emp next;
    public Emp(int id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
