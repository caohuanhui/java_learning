package pers.chh3213.google;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.google
 * @ClassName : HashTab.java
 * @createTime : 2022/2/16 11:36
 * @Email :
 * @Description :
 */
public class HashTab {
    private EmpLinkedList[] empLinkedLists;
    //表示有多少条链表
    private int size;

    public HashTab(int size) {
        this.size = size;
        empLinkedLists = new EmpLinkedList[size];
        //初始化，否则会报空指针异常
        for (int i = 0; i < size; i++) {
            empLinkedLists[i]=new EmpLinkedList();
        }
    }
    public int getHash(int id){
        //编写哈希函数，使用一个简单的取模法
        return id%size;
    }

    /**
     * 添加
     * @param emp
     */
    public void add(Emp emp){
        //根据员工的id,得到该员工应当添加到哪条链表
        int hashId = getHash(emp.id);
        empLinkedLists[hashId].add(emp);
    }

    /**
     * 遍历
     */
    public void list(){
        for (int i = 0; i < size; i++) {
            empLinkedLists[i].list(i);
        }
    }

    /**
     * 查找
     * @param id
     */
    public void findEmpById(int id){
        //使用散列函数确定到哪条链表查找
        int hashId = getHash(id);
        Emp emp = empLinkedLists[hashId].findEmpById(id);
        if(emp!=null){
            System.out.printf("在第%d条链表找到雇员:",hashId+1);
            System.out.println(emp);
        }else{
            System.out.println("未找到");
        }
    }

}
