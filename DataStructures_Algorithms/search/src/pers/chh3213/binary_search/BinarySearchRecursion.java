package pers.chh3213.binary_search;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_search
 * @ClassName : BinarySearchRecursion.java
 * @createTime : 2022/2/16 9.06
 * @Email :
 * @Description :二分查找,递归写法
 */
public class BinarySearchRecursion {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6,7,8,9};
        int left = 0, right=arr.length-1;
        int search = binarySearch(arr, 3,left,right);
        System.out.println(search);
    }
    public static int binarySearch(int[] arr, int findVal,int left,int right ){
        if(left>right) {
            return -1;
        }
        //防止溢出
        int mid = left+(right-left)/2;
        if(findVal>arr[mid]){
            return binarySearch(arr, findVal, mid+1, right);
        }
        else if(findVal<arr[mid]){
            return binarySearch(arr, findVal, left, mid-1);
        }
        else{
            return mid;
        }
    }
}
