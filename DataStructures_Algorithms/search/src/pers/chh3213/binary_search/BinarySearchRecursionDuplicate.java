package pers.chh3213.binary_search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_search
 * @ClassName : BinarySearchRecursionDuplicate.java
 * @createTime : 2022/2/16 9.06
 * @Email :
 * @Description :二分查找,递归写法,有重复值的情况
 */
public class BinarySearchRecursionDuplicate {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,4,4,4,5,5,6,7,8,9};
        int left = 0, right=arr.length-1;
        List<Integer> search = binarySearch(arr, 4, left, right);
        System.out.println(search);
    }
    public static List<Integer> binarySearch(int[] arr, int findVal, int left, int right ){
        if(left>right) {
            return new ArrayList<Integer>();
        }
        //防止溢出
        int mid = left+(right-left)/2;
        if(findVal>arr[mid]){
            return binarySearch(arr, findVal, mid+1, right);
        }
        else if(findVal<arr[mid]){
            return binarySearch(arr, findVal, left, mid-1);
        }
        else{
            ArrayList<Integer> arrayList = new ArrayList<>();
            arrayList.add(mid);
            //用于遍历mid左边的相同元素
            int leftIndex = mid - 1;
            while (leftIndex >= left && arr[leftIndex] == findVal) {
                arrayList.add(leftIndex);
                leftIndex--;
            }
            //用于遍历mid右边的相同元素
            int rightIndex = mid + 1;
            while (rightIndex <= right && arr[rightIndex] == findVal) {
                arrayList.add(rightIndex);
                rightIndex++;
            }
            return arrayList;
        }
    }
}
