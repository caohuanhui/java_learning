package pers.chh3213.binary_search;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_search
 * @ClassName : BinarySearch.java
 * @createTime : 2022/2/16 8:56
 * @Email :
 * @Description :二分查找,迭代写法
 */
public class BinarySearch {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6,7,8,9};
        int search = binarySearch(arr, 3);
        System.out.println(search);
    }
    public static int binarySearch(int[] arr, int findVal ){
        int left = 0, right=arr.length-1;
        while (left<=right){
            //防止溢出
            int mid = left+(right-left)/2;
            if(findVal>arr[mid]){
                left = mid+1;
            }
            else if(findVal<arr[mid]){
                right = mid-1;
            }
            else{
                return mid;
            }

        }
        return -1;
    }
}
