package pers.chh3213.binary_search;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_search
 * @ClassName : BinarySearchDuplicate.java
 * @createTime : 2022/2/16 9:15
 * @Email :
 * @Description :有重复值时的迭代二分查找
 */
public class BinarySearchDuplicate {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,4,4,5,5,5,6,7,8,9};
        List<Integer> searchList = binarySearch(arr, 4);
        if(searchList.size() == 0) {
            System.out.println("未找到该元素");
        }else {
            for(Integer index : searchList) {
                System.out.println(index);
            }
        }
    }
    public static List<Integer> binarySearch(int[]arr, int findVal) {
        int left = 0, right = arr.length - 1;
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (left <= right) {
            //防止溢出
            int mid = left + (right - left) / 2;
            if (findVal > arr[mid]) {
                left = mid + 1;
            } else if (findVal < arr[mid]) {
                right = mid - 1;
            } else {
                //将索引存入列表中
                arrayList.add(mid);
                //用于遍历mid左边的相同元素
                int leftIndex = mid - 1;
                while (leftIndex >= left && arr[leftIndex] == findVal) {
                    arrayList.add(leftIndex);
                    leftIndex--;
                }
                //用于遍历mid右边的相同元素
                int rightIndex = mid + 1;
                while (rightIndex <= right && arr[rightIndex] == findVal) {
                    arrayList.add(rightIndex);
                    rightIndex++;
                }
                return arrayList;
            }
        }
        return arrayList;
    }

}
