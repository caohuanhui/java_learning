package pers.chh3213.seq_search;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.seq_search
 * @ClassName : SeqSearch.java
 * @createTime : 2022/2/16 8:45
 * @Email :
 * @Description :线性查找
 */
public class SeqSearch {
    public static void main(String[] args) {
        int[] arr={100, 101, 1, 7, 5,12,0};
        int search = seqSearch(arr, 0);
        System.out.println(search);
    }
    public static int seqSearch(int[]arr, int num){
        for (int i = 0; i < arr.length; i++) {
            if(arr[i]==num)return i;
        }
        return -1;
    }
}
