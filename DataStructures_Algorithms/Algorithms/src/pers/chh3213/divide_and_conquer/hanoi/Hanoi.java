package pers.chh3213.divide_and_conquer.hanoi;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.divide_and_conquer.hanoi
 * @ClassName : Hanoi.java
 * @createTime : 2022/2/19 11:28
 * @Email :
 * @Description :
 */
public class Hanoi {
    public static void main(String[] args) {
        hanoiTower(2,'A','B','C');
    }
    /**
     * 汉诺塔
     * @param num 盘子总数
     * @param a 塔A
     * @param b 塔B
     * @param c 塔C
     */
    public static void hanoiTower(int num,char a,char b, char c){
        if(num==1) {
            System.out.printf("第%d个盘子从%s到%s",num,a,c);
            System.out.println();
        }
        else{
            //如果我们有n>=2情况,我们总是可以看做是两个盘1.最下边的一个盘2.上面的所有盘,
            //1.先把最上面的所有盘A->B, 移动过程会使用到c
            hanoiTower(num-1,a,c,b);
            //2.把最下面的盘从A移动到C
            System.out.printf("第%d个盘子从%s到%s",num,a,c);
            System.out.println();
            //3.再将B中的盘：B->C
            hanoiTower(num-1,b,a,c);
        }
    }
}
