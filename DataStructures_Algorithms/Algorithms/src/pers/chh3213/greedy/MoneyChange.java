package pers.chh3213.greedy;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.greedy
 * @ClassName : MoneyChange.java
 * @createTime : 2022/2/19 20:04
 * @Email :
 * @Description :
 * 假设纸币金额为1元、5元、10元、20元、50元、100元，
 * 用尽可能少的纸币数量凑成123元。
 */
public class MoneyChange {
    public static void main(String[] args) {
        int[] cases = {100,50, 20, 10, 5, 1};//需要从大到小排列
        int[] arrCases = change(123,cases);
        System.out.println(Arrays.toString(arrCases));
        //打印结果
        for(int i = 0; i<cases.length; i++) {
            if(arrCases[i] != 0) {
                System.out.println("需要" + cases[i] + "元的纸币" + arrCases[i] + "张");
            }
        }
    }

    /**
     * 凑钱
     * @param money 总金额
     * @param cases 纸币数组
     * @return 返回每张钱币要拿出的数量组成的数组
     */
    public static int[] change(int money,int[]cases){
        int leftMoney = money;
        int[] counter = new int[cases.length];
        if(money<0){
            System.out.println("输入金额为负数！！");
            return null;
        }
        while (leftMoney>0){
            for (int i = 0; i < cases.length; i++) {
                int count=0;
                while(leftMoney-cases[i]>=0){
                    count++;
                    leftMoney = leftMoney-cases[i];
                }
                counter[i]=count;
            }
        }
        return counter;
    }
}
