package pers.chh3213.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.greedy
 * @ClassName : GreedyAlgorithm.java
 * @createTime : 2022/2/19 19:26
 * @Email :
 * @Description :假设存在下面需要付费的广播台，以及广播台信号可以覆盖的地区。
 * 如何选择最少的广播台，让所有的地区都可以接收到信号
 * 电台 	覆盖地区个数 	覆盖地区
 * K1 	0 	北京 上海 天津
 * K2 	0 	广州 北京 深圳
 * K3 	0 	成都 上海 杭州
 * K4 	0 	上海 天津
 * K5 	0 	杭州 大连
 */
public class GreedyAlgorithm {
    /**
     *
     * @param allAreas 存放的所有地区
     * @param broadcasts  所有广播电台
     * @return
     */
    public ArrayList<String> greedyMethod(HashSet<String>allAreas, HashMap<String,HashSet<String>>broadcasts){
        //创建ArrayList,存放选择的电台集合
        ArrayList<String> selects = new ArrayList<>();
        //定义一个临时的集合,在遍历的过程中,
        //存放遍历过程中的电台覆盖的地区和当前还没有覆盖的地区的
        HashSet<String> tempSet = new HashSet<>();
        //定义给maxKey,保存在一次遍历过程中,能够覆盖最大未覆盖的地区对应的电台的key
        //如果maxKey不为null,则会加入到selects
        String maxKey=null;
        //如果allAreas不为0,则表示还没有覆盖到所有的地区
        while (allAreas.size()!=0){
            maxKey=null;
            //遍历broadcasts,取出对应 key
            for (String key:broadcasts.keySet()) {
                //每进行一次for
                tempSet.clear();
                //当前这个key能够覆盖的地区
                HashSet<String> areas = broadcasts.get(key);
                tempSet.addAll(areas);
                //求出tempSet和allAreas集合的交集,交集会赋给tempSet
                tempSet.retainAll(allAreas);
                //如果当前这个集合包含的未覆盖地区的数量,比maxKey指向的集合地区还多
                //就需要重置maxKey
                if(tempSet.size()>0&&(maxKey==null||tempSet.size()>broadcasts.get(maxKey).size())){
                    maxKey = key;
                }
            }
            //maxKey != null, 应该将maxKey加入 selects
            if(maxKey!=null){
                selects.add(maxKey);
                ///将maxKey指向的广播电台覆盖的地区,从allAreas去掉
                allAreas.removeAll(broadcasts.get(maxKey));
            }

        }
        return selects;
    }
}
