package pers.chh3213.greedy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.greedy
 * @ClassName : Demo.java
 * @createTime : 2022/2/19 19:45
 * @Email :
 * @Description :电台覆盖问题
 * 假设存在下面需要付费的广播台，以及广播台信号可以覆盖的地区。
 *  * 如何选择最少的广播台，让所有的地区都可以接收到信号
 *  * 电台 	覆盖地区个数 	覆盖地区
 *  * K1 	0 	北京 上海 天津
 *  * K2 	0 	广州 北京 深圳
 *  * K3 	0 	成都 上海 杭州
 *  * K4 	0 	上海 天津
 *  * K5 	0 	杭州 大连
 */
public class Demo {
    public static void main(String[] args) {
        HashMap<String, HashSet<String>> broadcasts = new HashMap<>();
        HashSet<String> K1 = addTel(new String[]{"北京", "上海", "天津"});
        HashSet<String> K2 = addTel(new String[]{"广州", "北京", "深圳"});
        HashSet<String> K3 = addTel(new String[]{"成都", "上海", "杭州"});
        HashSet<String> K4 = addTel(new String[]{"上海",  "天津"});
        HashSet<String> K5 = addTel(new String[]{"杭州", "大连"});
        broadcasts.put("K1",K1);
        broadcasts.put("K2",K2);
        broadcasts.put("K3",K3);
        broadcasts.put("K4",K4);
        broadcasts.put("K5",K5);
        //allAreas存放所有的地区
        HashSet<String> allAreas = new HashSet<>();
        allAreas.add("北京");
        allAreas.add("上海");
        allAreas.add("天津");
        allAreas.add("广州");
        allAreas.add("深圳");
        allAreas.add("成都");
        allAreas.add("杭州");
        allAreas.add("大连");

        GreedyAlgorithm greedyAlgorithm = new GreedyAlgorithm();
        ArrayList<String> arrayList = greedyAlgorithm.greedyMethod(allAreas, broadcasts);
        System.out.println("结果："+arrayList);//K1,K2,K3,K5

    }

    /**
     * 生成电台
     * @param strings 电台覆盖的地方数组
     * @return
     */
    public static HashSet<String> addTel(String[] strings){
        HashSet<String> hashSet = new HashSet<>();
        for (String str:strings) {
            hashSet.add(str);
        }
        return hashSet;
    }
}
