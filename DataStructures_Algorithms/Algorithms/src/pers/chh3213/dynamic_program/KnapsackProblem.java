package pers.chh3213.dynamic_program;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.dynamic_program
 * @ClassName : KnapsackProblem.java
 * @createTime : 2022/2/19 14:47
 * @Email :
 * @Description :动态规划求解背包问题
 * 物品 	重量 	价值
 * 吉他 	1 	1500
 * 音响 	4 	3000
 * 电脑 	3 	2000
 * 一个背包最多装4kg的东西，求
 *     装入物品使得背包的总价值最大，且不超出背包的容量
 *     要求装入的物品不能重复（01背包）
 */
public class KnapsackProblem {
    //存储最大价值
    private int[][] value;
    //用于表示物品放入背包的方式
    private int[][] method;
    //背包容量
    private int m;
    //物品个数
    private int n;

    public static void main(String[] args) {
        //各个物品的重量
        int[] w = {1,4,3};
        //物品价值
        int[] v = {1500,3000,2000};
        //背包容量
        int m = 4;
        //物品个数
        int n = v.length;
        KnapsackProblem knapsackProblem = new KnapsackProblem(m, n);
        knapsackProblem.DP(w,v);
    }
    public KnapsackProblem(int m, int n) {
        this.m = m;
        this.n = n;
        value = new int[n+1][m+1];
        method = new int[n+1][m+1];
    }

    /**
     * 动态规划
     * @param w 物品重量数组
     * @param v 物品价值数组
     */
    public  void DP(int[] w, int[]v){
        ///初始化第一行和第一列,这里在本程序中,可以不去处理,因为默认就是0
        for (int i = 0; i < value.length; i++) {
            value[i][0]=0;
        }
        for (int i = 0; i < value[0].length;i++) {
            value[0][i]=0;
        }
        //不处理第一行,i是从1开始的
        for (int i = 1; i < value.length; i++) {
            //不处理第一列,j是从1开始的
            for(int j = 1; j < value[0].length; j++){
                //因为我们程序i是从1开始的,因此原来公式中的w[i]修改成w[i-1]
                if(w[i-1]>j){
                    value[i][j]= value[i-1][j];
                }else{
                    //背包剩余的容量
                    int remain = j-w[i-1];
                    //如果放入该物品前的最大价值大于放入该物品后的最大价值，就不放入该物品
                    if(value[i-1][j]>v[i-1]+value[i-1][remain]){
                        value[i][j]=value[i-1][j];
                    }else{
                        value[i][j]=v[i-1]+value[i-1][remain];
                        //存入放入方法
                        method[i][j]=1;
                    }
                }
            }
        }
        //打印放入背包的最大价值
        for (int[] val:value) {
            System.out.println(Arrays.toString(val));
        }
        //打印价值最大的放法
        //存放方法的二维数组的最大下标，从最后开始搜索存放方法
        int i = method.length - 1;
        int j = method[0].length - 1;
        while(i > 0 && j > 0) {
            if(method[i][j] == 1) {
                System.out.println("将第" + i + "个物品放入背包");
                //背包剩余容量
                j -= w[i-1];
            }
            i--;
        }
    }

}
