package pers.chh3213.KMP;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.KMP
 * @ClassName : bruteForce.java
 * @createTime : 2022/2/19 15:31
 * @Email :
 * @Description :暴力解法解决字符串匹配问题
 */
public class bruteForce {
    public static void main(String[] args) {
        String str1="BBC ABCDAB ABCDABCDABDE";
        String str2="ABCDABD";
        int included = isIncluded(str1, str2);
        if(included!=-1){
            System.out.println("匹配成功");
            System.out.println("输出位置为"+included);
        }else{
            System.out.println("匹配失败");

        }
    }
    public static int isIncluded(String str1,String str2){
        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();
        for (int i = 0; i < charArray1.length; i++) {
            int index = i;
            int j;
            for (j = 0; j < charArray2.length&&index<charArray1.length;) {
                if(charArray1[index]==charArray2[j]){
                    index++;
                    j++;
                }else{
                    break;
                }
            }
            if(j==charArray2.length){
                return i;
            }
        }
        return -1;
    }
}