package pers.chh3213.KMP;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.KMP
 * @ClassName : KMPSearch.java
 * @createTime : 2022/2/19 16:37
 * @Email :
 * @Description :
 */
public class KMPSearch {
    public static void main(String[] args) {
        String str1="BBC ABCDAB ABCDABCDABDE";
        String str2="ABCDABD";
        int[] table = getTable(str2);
        System.out.println("部分匹配表:"+Arrays.toString(table));
        int included = kmp(str1, str2,table);
        if(included!=-1){
            System.out.println("匹配成功");
            System.out.println("输出位置为"+included);
        }else{
            System.out.println("匹配失败");

        }
    }

    /**
     * 得到字符串的部分匹配表
     * @param matchStr 用于匹配的字符串
     * @return 部分匹配表
     */
    public static int[] getTable(String matchStr){
        //部分匹配值的数组
        int[] partTable = new int[matchStr.length()];
        //字符串的第一个元素没有前缀与后缀，部分匹配值为0
        partTable[0]=0;
        //i用来指向部分匹配字符串末尾的字符，j用来指向开始的字符
        for (int i = 1,j=0; i < matchStr.length(); i++) {
            //当j>0且前缀后缀不匹配时
            while (j>0&&matchStr.charAt(i)!=matchStr.charAt(j)){
                //使用部分匹配表中前一个表项的值
                j = partTable[j-1];
            }
            //如果前缀后缀匹配，j向后移，继续比较
            if(matchStr.charAt(i)==matchStr.charAt(j)){
                j++;
            }
            //存入匹配值
            partTable[i]=j;
        }
        return partTable;
    }

    /**
     * kmp搜索算法
     * @param str1 原字符串
     * @param str2 字串
     * @param partTable 字串对应的部分匹配表
     * @return 如果是-1就是没有匹配到,否则返回第一个匹配的位置,
     */
    public static int kmp(String str1,String str2,int[] partTable){
        for (int i = 0,j=0; i < str1.length(); i++) {
            while (j>0&&str1.charAt(i)!=str2.charAt(j)){
                j=partTable[j-1];
            }
            if(str1.charAt(i)==str2.charAt(j)){
                j++;
            }
            if(j==str2.length()){
                //如果匹配完成，返回第一个字符出现位置
                return i-j+1;
            }
        }
        return -1;
    }
}
