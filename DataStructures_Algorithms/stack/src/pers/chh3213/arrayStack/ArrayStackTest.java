package pers.chh3213.arrayStack;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.arrayStack
 * @ClassName : ArrayStackTest.java
 * @createTime : 2022/2/14 9:24
 * @Email :
 * @Description :
 */
public class ArrayStackTest {
    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack(10);
        stack.push(7);
        stack.push(6);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.showStack();
        // 出栈
        System.out.println("================");
        stack.pop();
        stack.showStack();

    }
}
