package pers.chh3213.arrayStack;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.arrayStack
 * @ClassName : ArrayStack.java
 * @createTime : 2022/2/14 9:09
 * @Email :
 * @Description :定义一个数组栈
 */
public class ArrayStack {
    // 定义top，初始为-1
    private int top = -1;
    // 栈的大小
    private int maxSize;
    //数组模拟栈
    private int[] stack;

    /**
     * 构造器，初始化数组栈
     * @param maxSize
     */
    public ArrayStack(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    /**
     * 判断栈满
     * @return
     */
    public boolean isFull(){
        if(this.top == this.maxSize-1){
            return true;
        }
        return false;
    }

    /**
     * 判断栈空
     * @return
     */
    public boolean isEmpty(){
        if(this.top==-1){
            return true;
        }
        return false;
    }

    /**
     * 入栈
     * @param data
     */
    public void push(int data){
        if(isFull()){
            System.out.println("栈已满！");
            return;
        }
        top++;
        stack[top]=data;
    }

    /**
     * 出栈
     * @return
     */
    public int pop(){
        if(isEmpty()){
            throw new RuntimeException("栈已空");
        }
        int value = stack[top];
        top--;
        return value;
    }

    public void showStack(){
        if (isEmpty()){
            System.out.println("栈已空");
            return;
        }
        //从栈底开始显示数据
        //for (int value:stack
        //     ) {
        //    System.out.print(value+"\t");
        //}
        //System.out.println();

        // 从栈顶开始显示数据
        for (int i = top; i>-1; i--) {
            System.out.printf("stack[%d]=%d\n",i,stack[i]);
        }
    }
}
