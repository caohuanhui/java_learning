package pers.chh3213.expressionConvert;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.expressionConvert
 * @ClassName : ArrayStack3.java
 * @createTime : 2022/2/14 12:36
 * @Email :
 * @Description :定义一个数组栈
 */
public class ArrayStack3 {
    // 定义top，初始为-1
    private int top = -1;
    // 栈的大小
    private int maxSize;
    //数组模拟栈
    private int[] stack;

    /**
     * 构造器，初始化数组栈
     * @param maxSize
     */
    public ArrayStack3(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    /**
     * 返回当前栈顶的值
     * @return
     */
    public int peek(){
        return stack[top];
    }
    /**
     * 判断栈满
     * @return
     */
    public boolean isFull(){
        if(this.top == this.maxSize-1){
            return true;
        }
        return false;
    }

    /**
     * 判断栈空
     * @return
     */
    public boolean isEmpty(){
        if(this.top==-1){
            return true;
        }
        return false;
    }

    /**
     * 入栈
     * @param data
     */
    public void push(int data){
        if(isFull()){
            System.out.println("栈已满！");
            return;
        }
        top++;
        stack[top]=data;
    }

    /**
     * 出栈
     * @return
     */
    public int pop(){
        if(isEmpty()){
            throw new RuntimeException("栈已空");
        }
        int value = stack[top];
        top--;
        return value;
    }

    /**
     * 显示数据
     */
    public void showStack(){
        if (isEmpty()){
            System.out.println("栈已空");
            return;
        }
        for (int i = 0; i<=top; i++) {
            System.out.print((char)(stack[i]));
        }
        System.out.println();
    }

    /**
     * 返回运算符的优先级,优先级是程序员来确定,优先级使用数字表示
     * 数字越大,则优先级就越高.
     *
     * @param operation
     * @return
     */
    public int getPriority(int operation) {
        if (operation == '*' || operation == '/') {
            return 1;
        } else if (operation == '+' || operation == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     *
     * @param operation 传入字符
     * @return true则是传入字符优先级大于栈顶字符，false反之
     */
    public boolean comparePriority(int operation) {
        //System.out.println(operation);
        int priority1 = getPriority(operation);
        int priority2 = getPriority(peek());
        return priority1 > priority2;
    }

    /**
     * 判断是不是运算符
     * @param ch
     * @return
     */
    public boolean isOper(char ch){
        return ch=='+'||ch=='-'||ch=='*'||ch=='/';
    }


    /**
     * 判断元素类型
     * @param ch
     * @return 运算符返回3，左括号返回2，右括号返回1，数字返回0
     */
    public int judgeElement(char ch){
        if(isOper(ch)){
            return 3;
        }
        if(ch=='('){
            return 2;
        }
        if(ch==')'){
            return 1;
        }else{
            return 0;
        }
    }

}
