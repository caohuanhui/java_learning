package pers.chh3213.expressionConvert;

import pers.chh3213.arrayStack2.ArrayStack2;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.expressionConvert
 * @ClassName : ExpressionConvert.java
 * @createTime : 2022/2/14 12:36
 * @Email :
 * @Description :
 */
public class ExpressionConvert {
    public static void main(String[] args) {
        //初始化两个栈:运算符栈s1和储存中间结果的栈s2;
        ArrayStack3 operation_stack = new ArrayStack3(100);
        ArrayStack3 temp_stack = new ArrayStack3(100);
        String expression = "1+((2+3)*4)-5";
        int s1top = 0;
        //从左至右扫描中缀表达式
        for (int i = 0; i < expression.length(); i++) {
            char ch = expression.charAt(i);
            int type = temp_stack.judgeElement(ch);
            //遇到操作数时,将其压s2;
            if(type==0){
                temp_stack.push(ch);
            }
            //左括号，直接压入s1
            else if(type==2){
                operation_stack.push(ch);
            }
            // 如果是右括号“)",则依次弹出s1栈顶的运算符,并压入s2,直到遇到左括号为止,此时将这一对括号丢弃
            else if(type==1){
                do{
                    s1top = operation_stack.pop();
                    if(s1top=='('){
                        break;
                    }
                   temp_stack.push(s1top);
                }while (s1top!='(');
            }
            else{//遇到运算符时,比较其与s1栈顶运算符的优先级:
                //如果s1为空,或栈顶运算符为左括号“(",则直接将此运算符入栈;
                if(operation_stack.isEmpty()||operation_stack.peek()=='('){
                    operation_stack.push(ch);
                }else{
                    //该运算符优先级高于栈顶元素优先级，则入栈
                    if(operation_stack.comparePriority(ch)){
                        operation_stack.push(ch);
                    }else{
                        int s1peek = operation_stack.pop();
                        temp_stack.push(s1peek);
                        //重新判断该运算符
                        i--;
                    }
                }
            }
        }
        int last = operation_stack.pop();
        temp_stack.push(last);
        temp_stack.showStack();
    }
}
