package pers.chh3213.polandCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.polandCalculator
 * @ClassName : PolandCalculator.java
 * @createTime : 2022/2/14 13:48
 * @Email :
 * @Description :
 * 逆波兰计算器
 * 输入一个逆波兰表达式(后缀表达式),使用栈(Stack),计算其结果
 */
public class PolandCalculator {
    public static void main(String[] args) {
        //后缀表达式
        //为方便起见，数字和符号使用空格隔开
        String expression = "4 5 * 8 - 60 + 8 2 / +";
        List<String> listString = getListString(expression);
        System.out.println(listString);
        int result = calculate(listString);
        System.out.println("运算结果："+result);
    }

    /**
     * 将一个逆波兰表达式,依次将数据和运算符放入到ArrayList中
     * @param expression
     * @return
     */
    public static List<String> getListString(String expression){
        String[] split = expression.split(" ");
        ArrayList<String> list = new ArrayList<>();
        for (String s:split
             ) {
            list.add(s);
        }
        return list;
    }

    /**
     * 后缀表达式计算
     * @param list
     * @return
     */
    public static int calculate(List<String>list){
        Stack<String> stack = new Stack<>();
        for (String item:list
             ) {
            //这里使用正则表达式来取出数
            if(item.matches("\\d+")){//如果是数字,则直接入栈
                stack.push(item);
            }else{//如果是运算符
                //System.out.println(item);
                // pop出两个数,并运算, 再入栈
                int x1 = Integer.parseInt(stack.pop());
                int x2 = Integer.parseInt(stack.pop());
                int result = 0;
                if(item.equals("+")){
                    result =x1+x2;
                }
                else if(item.equals("-")){
                    result =x2-x1;
                }
                else if(item.equals("*")){
                    result =x2*x1;
                }
                else if(item.equals("/")){
                    result =x2/x1;
                }else {
                    throw new RuntimeException("运算符不正确");
                }
                stack.push(result+"");
            }
        }
        return Integer.parseInt(stack.peek());
    }
}
