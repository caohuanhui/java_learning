package pers.chh3213.arrayStack2;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.arrayStack
 * @ClassName : ArrayStack2.java
 * @createTime : 2022/2/14 9:09
 * @Email :
 * @Description :定义一个数组栈
 */
public class ArrayStack2 {
    // 定义top，初始为-1
    private int top = -1;
    // 栈的大小
    private int maxSize;
    //数组模拟栈
    private int[] stack;

    /**
     * 构造器，初始化数组栈
     * @param maxSize
     */
    public ArrayStack2(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    /**
     * 增加一个方法,可以返回当前栈顶的值,但是不是真正的pop
     * @return
     */
    private int peek(){
        return stack[top];
    }
    /**
     * 判断栈满
     * @return
     */
    public boolean isFull(){
        if(this.top == this.maxSize-1){
            return true;
        }
        return false;
    }

    /**
     * 判断栈空
     * @return
     */
    public boolean isEmpty(){
        if(this.top==-1){
            return true;
        }
        return false;
    }

    /**
     * 入栈
     * @param data
     */
    public void push(int data){
        if(isFull()){
            System.out.println("栈已满！");
            return;
        }
        top++;
        stack[top]=data;
    }

    /**
     * 出栈
     * @return
     */
    public int pop(){
        if(isEmpty()){
            throw new RuntimeException("栈已空");
        }
        int value = stack[top];
        top--;
        return value;
    }

    public void showStack(){
        if (isEmpty()){
            System.out.println("栈已空");
            return;
        }
        //从栈底开始显示数据
        //for (int value:stack
        //     ) {
        //    System.out.print(value+"\t");
        //}
        //System.out.println();

        // 从栈顶开始显示数据
        for (int i = top; i>-1; i--) {
            System.out.printf("stack[%d]=%d\n",i,stack[i]);
        }
    }

    /**
     * 返回运算符的优先级,优先级是程序员来确定,优先级使用数字表示
     * 数字越大,则优先级就越高.
     * 假设目前只有+-/*
     * @param operation
     * @return
     */
    public int getPriority(int operation) {
        if (operation == '*' || operation == '/') {
            return 1;
        } else if (operation == '+' || operation == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     *
     * @param operation 传入字符
     * @return true则是传入字符优先级大于栈顶字符，false反之
     */
    public boolean comparePriority(int operation) {
        //System.out.println(operation);
        int priority1 = getPriority(operation);
        int priority2 = getPriority(peek());
        return priority1 > priority2;
    }

    /**
     * 判断是不是运算符
     * @param ch
     * @return
     */
    public boolean isOper(char ch){
        return ch=='+'||ch=='-'||ch=='*'||ch=='/';
    }
    /**
     * 计算方法
     * @param number1   第一个运算的数字
     * @param number2   第二个运算的数字
     * @param operation 运算符
     * @return
     */
    public int calculation(int number1, int number2, int operation) {
        switch (operation) {
            case '+':
                return number1+number2;
            case '-':
                //注意顺序
                return number2-number1;
            case '*':
                return number1*number2;
            case '/':
                return number2/number1;
            default:
                System.out.println(operation);
                throw new RuntimeException("符号读取错误！");
        }
    }
}
