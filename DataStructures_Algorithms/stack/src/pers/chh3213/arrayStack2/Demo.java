package pers.chh3213.arrayStack2;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.arrayStack2
 * @ClassName : Demo.java
 * @createTime : 2022/2/14 10:01
 * @Email :
 * @Description :表达式计算值
 */
public class Demo {
    public static void main(String[] args) {
        String expression = "7*2*2-5+1-5+3-100";
        //数栈与符号栈
        ArrayStack2 num_stack = new ArrayStack2(10);
        ArrayStack2 operation_stack = new ArrayStack2(10);
        //索引，用来读取字符串中的元素
        int index = 0;
        //保存读取到的数字和符号
        int number1 = 0;
        int number2 = 0;
        int oper = 0;
        // 保存当前位置的元素
        char ch = ' ';
        //用于拼接数字
        String spliceNumber = "";
        //保存运算结果
        int result=0;
        for ( index = 0; index < expression.length(); index++) {
            ch = expression.charAt(index);
            // 如果是运算符
            if(operation_stack.isOper(ch)){
                // 如果当前的符号栈为空,就直接入栈
                if(operation_stack.isEmpty()){
                    operation_stack.push(ch);
                }
                else{//如果当前的符号栈不为空
                    // 如果index位置上的操作符的优先级大于栈顶的操作符优先级，就直接入符号栈
                    if(operation_stack.comparePriority(ch)){
                        operation_stack.push(ch);
                    }
                    else {
                        //如果index位置上的操作符的优先级小于或者等于栈顶的操作符优先级
                        //就需要从数栈中pop出两个数，再从符号栈中pop出一个符号,进行运算，
                        // 将运算结果放入数栈中，并将index位置上的符号压入符号栈；
                        oper = operation_stack.pop();
                        number1 = num_stack.pop();
                        number2 = num_stack.pop();
                        result = operation_stack.calculation(number1,number2,oper);
                        num_stack.push(result);
                        operation_stack.push(ch);
                    }
                }
            }
            else{//如果是数字，就一直读取,入数栈
                while (ch>='0'&&ch<='9'){
                    //可能该数字为多位数，所以不能只存入一位数字
                    spliceNumber+=ch;
                    index++;
                    //如果已经读了最后一个数字了，就停下来
                    if(index >= expression.length()) {
                        break;
                    }
                    ch = expression.charAt(index);
                }
                System.out.println("拼接字符: " + spliceNumber);
                int number = Integer.parseInt(spliceNumber);
                num_stack.push(number);
                // 清空，初始化
                spliceNumber="";
                index--;
            }
        }
        /*
         * 当表达式扫描完毕,就顺序的从数栈和符号栈中pop出相应的数和符号,并运行.
         */
        while (!operation_stack.isEmpty()){
            number1 = num_stack.pop();
            number2 = num_stack.pop();
            oper = operation_stack.pop();
            result = operation_stack.calculation(number1,number2,oper);
            num_stack.push(result);
        }
        //将数栈的最后数, pop出,就是结果
        System.out.printf("表达式 %s = %d",expression,num_stack.pop());

    }
}
