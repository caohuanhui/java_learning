package pers.chh3213.queen8;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.queen8
 * @ClassName : Queen8Test.java
 * @createTime : 2022/2/14 20:55
 * @Email :
 * @Description :
 */
public class Queen8Test {
    public static void main(String[] args) {
        Queen8 queen8 = new Queen8(8);
        queen8.check(0);
        System.out.printf("一共有%d种解法\n",queen8.count);//92
        System.out.printf("一共%d次判断冲突\n",queen8.judge_count);//15720
    }
}
