package pers.chh3213.queen8;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.queen8
 * @ClassName : Queen8.java
 * @createTime : 2022/2/14 20:27
 * @Email :
 * @Description :
 */
public class Queen8 {
    // 皇后数量
    int max = 8;
    // 皇后数组
    int[] queen_pos;
    // 判断冲突的次数
    int judge_count=0;
    //总共有count种放法
    int count=0;
    public Queen8(int max) {
        this.max = max;
        queen_pos = new int[max];
    }

    /**
     * 打印8皇后位置数组元素
     */
    public void showQueen(){
        count++;
        for (int i = 0; i < queen_pos.length; i++) {
            System.out.print(queen_pos[i]+"\t");
        }
        System.out.println();
    }

    /**
     * 判断该位置的皇后与前面几个是否冲突
     * @param pos 需要判断的皇后的位置
     * @return  true代表冲突，false代表不冲突
     */
    public boolean judge(int pos){
        judge_count++;
        for (int i = 0; i < pos; i++) {
            //如果两个皇后在同一列或者同一斜线，就冲突
            //因为数组下标代表行数，所以不会存在皇后在同一行
            //所在行数-所在行数 如果等于 所在列数-所在列数，则两个皇后在同一斜线上
            if(queen_pos[i]==queen_pos[pos]){
                return true;
            }
            if(pos-i==Math.abs(queen_pos[pos]-queen_pos[i])){
                return true;
            }
        }
        return false;
    }

    /**
     * 检查该皇后应该放的位置
     */
    public void check(int queen){
        if(queen==max){
            //所有的皇后都放好了，打印并返回
            showQueen();
            return;
        }
        //把皇后放在每一列上，看哪些不会和之前的冲突
        for (int i = 0; i < max; i++) {
            //先把当前这个皇后queen,放到该行的第1列
            queen_pos[queen]=i;
            if(!judge(queen)){
                //不冲突，就去放下一个皇后
                check(queen+1);
            }
            //如果冲突,就继续执行array[queen]=i;即将第queen个皇后,放置在本行的后移的一个位置
        }
    }
}
