package pers.chh3213.migong;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.migong
 * @ClassName : Maze.java
 * @createTime : 2022/2/14 19:35
 * @Email :
 * @Description :
 *   -  用一个二维矩阵map代表地图
 *         0：代表未走过该地点
 *         1：代表边界
 *         2：代表走过且能走得通
 *         3：代表走过但走不通
 *    - 设置起点和终点以及每个地点的行走策略
 *         行走策略指在该点所走的方向的顺序，如 下右上左（调用寻找路径的方法，使用递归）
 *    - 每次行走时假设该点能够走通，然后按照策略去判断，如果所有策略判断后都走不通，则该点走不通
 */
public class Maze {
    int[][]map;
    int len;
    int width;

    public Maze(int length, int width) {
        this.len = length;
        this.width = width;
        map = new int[length][width];
    }

    /**
     * 设置地图
     * @return
     */
    public int[][] getMap(){
        //设置边界
        for (int i = 0; i < len; i++) {
            map[i][0] = 1;
            map[i][width-1] = 1;
        }
        for (int i = 0; i < width; i++) {
            map[0][i]=1;
            map[len-1][i]=1;
        }
        //设置挡板
        map[3][1]=1;
        map[3][2]=1;
        //map[1][2]=1;
        //map[2][2]=1;
        return map;
    }
    /**
     * 查看地图
     */
    public void showMap(){
        System.out.println("========map==========");
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print(map[i][j]+"\t");
            }
            System.out.println();
        }
    }

    /**
     * 使用递归找路
     * @param i 初始位置x
     * @param j 初始位置y
     */
    public boolean setWay(int i, int j){
        //假设右下角为终点
        if(map[len-2][width-2]==2){
            //通路已找到，返回true
            return true;
        }else{
            //按照下-右-上-左 走
            if(map[i][j]==0){
                //如果当前点没有走过
                //假设路能走通
                map[i][j]=2;
                if(setWay(i+1,j)){
                    return true;
                }else if(setWay(i,j+1)){
                    return true;
                }else if(setWay(i-1,j)){
                    return true;
                }else if(setWay(i,j-1)){
                    return true;
                }else{
                    // 否则认为该点走不通
                    map[i][j]=3;
                    return false;
                }
            }else{
                //如果该点已经被标记过了，不用再走了，直接返回false
                return false;
            }
        }
    }
}
