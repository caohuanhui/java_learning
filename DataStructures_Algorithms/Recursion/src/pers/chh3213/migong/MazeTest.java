package pers.chh3213.migong;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.migong
 * @ClassName : MazeTest.java
 * @createTime : 2022/2/14 20:00
 * @Email :
 * @Description :
 */
public class MazeTest {
    public static void main(String[] args) {
        Maze maze = new Maze(8, 7);
        int[][] map = maze.getMap();
        maze.showMap();
        maze.setWay(2,2);
        maze.showMap();
    }
}
