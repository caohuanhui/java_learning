package pers.chh3213.Queue;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.Queue
 * @ClassName : CircleArrayQueue.java
 * @createTime : 2022/1/27 14:28
 * @Email :
 * @Description :
 */
public class CircleArrayQueue {
    //队列的最大容量
    private int maxSize;
    //头指针
    private int front;
    //尾指针
    private int rear;
    //存放数据
    private int[] arr;
    public static void main(String[] args) {
        //此时队列可存储的有效数据个数为5
        ArrayQueue arrayQueue = new ArrayQueue(6);
        arrayQueue.addQueue(12);
        arrayQueue.addQueue(16);
        arrayQueue.addQueue(14);
        arrayQueue.addQueue(15);
        arrayQueue.addQueue(17);
        System.out.println("=======1.显示队列");
        arrayQueue.showQueue();
        System.out.println();
        System.out.println("=========2.显示头元素");
        arrayQueue.headQueue();
        System.out.println("========3.出列");
        arrayQueue.getQueue();
        System.out.println("=========4.显示队列");
        arrayQueue.showQueue();
        System.out.println();
        System.out.println("=========5.进入队列");
        arrayQueue.addQueue(14);
        System.out.println("=========4.显示队列");
        arrayQueue.showQueue();
    }
    public CircleArrayQueue(int maxSize) {
        this.maxSize = maxSize;
        //指向队列头,
        this.front = 0;
        //指向队列尾
        this.rear = 0;
        this.arr = new int[maxSize];
    }
    /**
     * 判断队列是否满了
     * @return
     */
    public boolean isFull(){
        return front==(rear +1) % maxSize;
    }
    /**
     * 判断队列是否是空的
     * @return
     */
    public boolean isNull(){
        return front==rear;
    }
    /**
     * 往队列添加元素
     * @param element
     */
    public void addQueue(int element){
        if(!isFull()){
            arr[rear]=element;
            //将rear后移，取模的方式
            rear = (rear+1)%maxSize;
        }
        else{
            System.out.println("队列已满");
        }
    }
    /**
     * 出队列，获取队列数据
     */
    public int getQueue(){
        if(!isNull()){
            int value = arr[front];
            System.out.println("出队列元素为："+value);
            front = (front+1)%maxSize;
            return value;
        }else{
            throw new RuntimeException("队列已空");
        }
    }
    public int showSize(){
        return (rear+ maxSize-front) % maxSize;
    }
    /**
     * 显示队列的情况
     */
    public void showQueue(){
        if(!isNull()){
            for (int i = front; i < front+showSize(); i++) {
                System.out.print(arr[i%maxSize]+"\t");
            }
        }else{
            throw new RuntimeException("队列已空");
        }
    }
    /**
     *  查看队列头元素
     * @return
     */
    public int headQueue(){
        if(!isNull()){
            System.out.println(arr[front]);
            return arr[front];
        }
        else {
            throw new RuntimeException("队列已空");
        }
    }
    /**
     * 退出系统
     */
    public void exitSystem(){
        System.exit(0);
    }

}
