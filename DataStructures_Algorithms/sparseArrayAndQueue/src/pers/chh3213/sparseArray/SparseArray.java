package pers.chh3213.sparseArray;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.sparseArray
 * @ClassName : SparseArray.java
 * @createTime : 2022/1/27 10:37
 * @Email :
 * @Description :
 */
public class SparseArray {
    public static void main(String[] args) {
        int[][] arr = new int[11][11];
        arr[1][2]=1;
        arr[2][3]=2;
        // 输出原始二维数组
        System.out.println("========原始数组==========");
        for (int[] a:
             arr) {
            for (int aa:
                 a) {
                System.out.print(aa+"\t");
            }
            System.out.println();
        }
        //二维--》稀疏
        System.out.println("=======稀疏数组======");
        int[][] sparse = array2Sparse(arr);
        for (int[]a:
             sparse) {
            System.out.println(a[0]+"\t"+a[1]+"\t"+a[2]);
        }
        //稀疏--》二维
        System.out.println("=========二维数组========");
        int[][] arr2 = sparse2Array(sparse);
        for (int[] a:
                arr2) {
            for (int aa:
                    a) {
                System.out.print(aa+"\t");
            }
            System.out.println();
        }
        File file = new File("E:\\CHH3213_KING\\研究生\\导师\\就业规划\\Java_Learning\\DataStructures_Algorithms\\sparseArrayAndQueue\\src\\pers\\chh3213\\sparseArray\\sparse.txt");
        //saveSparse(sparse,file);
        int[][] sparse1 = readSparse(file);
        System.out.println("====文件读取========");
        for (int[]a:
                sparse1) {
            System.out.println(a[0]+"\t"+a[1]+"\t"+a[2]);
        }
    }

    /**
     * 二维数组 转 稀疏数组
     * @param arr 二维数组
     * @return 稀疏数组
     */
    public static int[][] array2Sparse(int[][] arr){
        //1.遍历原始的二维数组，得到有效数据的个数 sum
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(arr[i][j]!=0){
                    sum+=1;
                }
            }
        }
        // 2.创建稀疏数组
        int[][] sparse_arr = new int[sum+1][3];
        // 保存原始数据的维度信息和有效数据个数
        sparse_arr[0] = new int[]{arr.length,arr[0].length,sum};
        //3. 将二维数组的有效数据信息存入到 稀疏数组
        int count=1;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if(arr[i][j]!=0){
                    sparse_arr[count++] = new int[]{i,j,arr[i][j]};
                }
            }
        }
        return sparse_arr;
    }

    /**
     * 稀疏数组--》二维数组
     * @param sparse_arr 稀疏数组
     * @return 二维数组
     */
    public static int[][] sparse2Array(int[][]sparse_arr){
        //1. 先读取稀疏数组的第一行，根据第一行的数据，创建原始的二维数组
        int[][] arr = new int[sparse_arr[0][0]][sparse_arr[0][1]];
        //System.out.println(sparse_arr[0][0]);
        //System.out.println(sparse_arr[0][1]);
        //2. 在读取稀疏数组后几行的数据，并赋给 原始的二维数组 即可.
        for (int i = 1; i < sparse_arr.length; i++) {
            arr[sparse_arr[i][0]][sparse_arr[i][1]]=sparse_arr[i][2];
        }
        return arr;
    }

    /**
     * 将稀疏数组包保存到磁盘文件中：
     * @param sparse 稀疏数组
     * @param file 保存的文件
     */
    public static void saveSparse(int[][] sparse, File file){
        FileWriter fileWriter = null;
        try {
            //File file = new File("E:\\CHH3213_KING\\研究生\\导师\\就业规划\\Java_Learning\\DataStructures_Algorithms\\sparseArrayAndQueue\\src\\pers\\chh3213\\sparseArray\\sparse.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            fileWriter = new FileWriter(file);
            for (int i = 0; i < sparse.length; i++) {
                for (int j = 0; j < sparse[i].length; j++) {
                    String s = String.valueOf(sparse[i][j]) + " ";
                    fileWriter.write(s);

                }
                fileWriter.write("\r\n");
            }


        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(fileWriter!=null){
                try {
                    fileWriter.close();

                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 从文件中读取
     * @param file 文件名
     * @return 稀疏数组
     */
    public static int[][] readSparse(File file){
        BufferedReader bufferedReader = null;
        int[][] sparse = new int[10][3];
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            String line = null;
            int i=0;
            while((line=bufferedReader.readLine())!=null){
                //将按行读取的字符串按空格分割，得到一个string数组
                String[] strings = line.split(" ");
                //依次转换为int类型存入到分配好空间的数组中
                for (int j = 0; j < strings.length; j++) {
                    sparse[i][j]  = Integer.valueOf(strings[j]);

                }
                i++;
            }


        }catch (IOException e){
            e.printStackTrace();
        }finally {
            if(bufferedReader!=null){
                try {
                    bufferedReader.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            return sparse;

        }
    }
}
