package pers.chh3213.Depth_First_Search;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.Depth_First_Search
 * @ClassName : Graph.java
 * @createTime : 2022/2/19 9:19
 * @Email :
 * @Description :使用深度优先遍历搜索图
 */
public class Graph {
    public static void main(String[] args) {
        String[] vertex = {"1","2","3","4","5","6","7","8"};
        Graph graph = new Graph(vertex.length);
        //插入图的顶点
        for (int i = 0; i < vertex.length; i++) {
            graph.insertvertex(vertex[i]);
        }
        //指明连接的边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);
        graph.insertEdge(3,7,1);
        graph.insertEdge(4,7,1);
        graph.insertEdge(2,5,1);
        graph.insertEdge(2,6,1);
        graph.insertEdge(5,6,1);

        //显示图的邻接矩阵
        graph.showGraph();
        //深度优先遍历
        graph.dfs();//1 2 4 8 5 3 6 7
    }
    //存储顶点String
    ArrayList<String> vertexList;
    //保存矩阵
    int[][]edges;
    //边的数量
    int numOfEdges;
    //顶点总数;
    int numOfvertex;
    //存储顶点是否被访问
    boolean[] isVisited;

    /**
     * 根据顶点总数进行初始化
     * @param numOfvertex
     */
    public Graph(int numOfvertex) {
        this.numOfvertex = numOfvertex;
        vertexList = new ArrayList<>(numOfvertex);
        edges = new int[numOfvertex][numOfvertex];
        isVisited = new boolean[numOfvertex];
        numOfEdges=0;
    }

    /**
     * 插入顶点
     * @param vertex
     */
    public void insertvertex(String vertex){
        vertexList.add(vertex);
    }

    /**
     * 创建边
     * @param v1 表示点的下标即使第几个顶点 "A"-"B" "A"->0 "B"->1
     * @param v2 第二个顶点对应的下标
     * @param weight 表示0或1
     */
    public void insertEdge(int v1,int v2,int weight){
        edges[v1][v2]=weight;
        edges[v2][v1]=weight;
        numOfEdges++;
    }

    /**
     * 显示图
     */
    public void showGraph(){
        for (int[] list:edges) {
            System.out.println(Arrays.toString(list));
        }
    }

    /**
     * /得到第一个邻接结点的下标w
     * @param index 顶点下表
     * @return 如果存在就返回对应的下标,否则返回-1
     */
    public int getFirstNeighbor(int index){
        for (int i = 0; i < vertexList.size(); i++) {
            if(edges[index][i]>0)return i;
        }
        return -1;
    }

    /**
     * //根据前一个邻接结点的下标来获取下一个邻接结点
     * @param v1
     * @param v2
     * @return 如果存在就返回对应的下标,否则返回-1
     */
    public int getNextNeighbor(int v1, int v2){
        for (int i = v2+1; i < vertexList.size(); i++) {
            if(edges[v1][i]>0)return i;
        }
        return -1;
    }
    /**
     * 深度优先遍历
     * @param vertex 开始遍历的顶点下标
     */
    public void DFS(int vertex){
        //首先我们访问该结点,输出
        System.out.print(vertexList.get(vertex)+"->");
        //将结点设置为已经访问
        isVisited[vertex]=true;
        ///查找结点i的第一个邻接结点w
        int w = getFirstNeighbor(vertex);
        while (w!=-1){
            if(!isVisited[w])DFS(w);
            //如果w结点己经被访问过
            else {
                w=getNextNeighbor(vertex,w);
            }
        }
    }
    public void DFS(){
        //对未被访问的顶点都进行深度优先遍历
        for (int i = 0; i < vertexList.size(); i++) {
            if(!isVisited[i])DFS(i);
        }
    }
    /**
     * 换种写法:深度优先遍历
     * @param vertex 开始顶点下标
     */
    private void dfs(int vertex) {
        //被访问过了就返回
        if(isVisited[vertex]) {
            return;
        }
        System.out.print(vertexList.get(vertex) + "->");
        //标记已被访问
        isVisited[vertex] = true;
        //从相邻顶点开始深度优先遍历
        for(int nextVertex = vertex+1; nextVertex<vertexList.size(); nextVertex++) {
            //如果该顶点存在且未被访问，就向下访问
            if(edges[vertex][nextVertex] != 0 && !isVisited[nextVertex] ) {
                dfs(nextVertex);
            }
        }
    }

    public void dfs() {
        //对未被访问的顶点都进行深度优先遍历
        for(int i = 0; i<vertexList.size(); i++) {
            if(!isVisited[i]) {
                dfs(i);
            }
        }
    }
}
