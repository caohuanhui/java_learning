package pers.chh3213.Broad_First_Search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.Depth_First_Search
 * @ClassName : Graph.java
 * @createTime : 2022/2/19 9:19
 * @Email :
 * @Description :使用深度优先遍历搜索图
 */
public class Graph {
    public static void main(String[] args) {
        String[] vertex = {"1","2","3","4","5","6","7","8"};
        Graph graph = new Graph(vertex.length);
        //插入图的顶点
        for (int i = 0; i < vertex.length; i++) {
            graph.insertvertex(vertex[i]);
        }
        //指明连接的边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);
        graph.insertEdge(3,7,1);
        graph.insertEdge(4,7,1);
        graph.insertEdge(2,5,1);
        graph.insertEdge(2,6,1);
        graph.insertEdge(5,6,1);

        //显示图的邻接矩阵
        graph.showGraph();
        //广度优先遍历
        //graph.BFS();//1 2 3 4 5 6 7 8
        graph.bfs();//1 2 3 4 5 6 7 8
    }
    //存储顶点String
    ArrayList<String> vertexList;
    //保存矩阵
    int[][]edges;
    //边的数量
    int numOfEdges;
    //顶点总数;
    int numOfvertex;
    //存储顶点是否被访问
    boolean[] isVisited;
    /**
     * 用于保存访问过的顶点
     */
    private Queue<Integer> queue;

    /**
     * 根据顶点总数进行初始化
     * @param numOfvertex
     */
    public Graph(int numOfvertex) {
        this.numOfvertex = numOfvertex;
        vertexList = new ArrayList<>(numOfvertex);
        edges = new int[numOfvertex][numOfvertex];
        isVisited = new boolean[numOfvertex];
        queue = new LinkedList<Integer>();
        numOfEdges=0;
    }

    /**
     * 插入顶点
     * @param vertex
     */
    public void insertvertex(String vertex){
        vertexList.add(vertex);
    }

    /**
     * 创建边
     * @param v1 表示点的下标即使第几个顶点 "A"-"B" "A"->0 "B"->1
     * @param v2 第二个顶点对应的下标
     * @param weight 表示0或1
     */
    public void insertEdge(int v1,int v2,int weight){
        edges[v1][v2]=weight;
        edges[v2][v1]=weight;
        numOfEdges++;
    }

    /**
     * 显示图
     */
    public void showGraph(){
        for (int[] list:edges) {
            System.out.println(Arrays.toString(list));
        }
    }

    /**
     * /得到第一个邻接结点的下标w
     * @param index 顶点下表
     * @return 如果存在就返回对应的下标,否则返回-1
     */
    public int getFirstNeighbor(int index){
        for (int i = 0; i < vertexList.size(); i++) {
            if(edges[index][i]>0)return i;
        }
        return -1;
    }

    /**
     * //根据前一个邻接结点的下标来获取下一个邻接结点
     * @param v1
     * @param v2
     * @return 如果存在就返回对应的下标,否则返回-1
     */
    public int getNextNeighbor(int v1, int v2){
        for (int i = v2+1; i < vertexList.size(); i++) {
            if(edges[v1][i]>0)return i;
        }
        return -1;
    }

    /**
     * 多一个顶点进行广度优先遍历
     * @param vertex 顶点下标
     */
    private void BFS(int vertex){
        //表示队列的头结点对应下标
        int u;
        //邻接节点w
        int w;
        //11访问结点,输出结点信息
        System.out.print(vertexList.get(vertex)+"->");
        isVisited[vertex]=true;
        //将节点加入队列
        queue.add(vertex);
        while (!queue.isEmpty()){
            //取出队列的头结点下标
            u = queue.remove();
            //得到第一个邻接结点的下标w
            w = getFirstNeighbor(u);
            while (w!=-1){
                if(!isVisited[w]){
                    System.out.print(vertexList.get(w)+"->");
                    isVisited[w]=true;
                    queue.add(w);
                }
                //以u为前驱点,找w后面的下一个邻结点
                w=getNextNeighbor(u,w);
            }
        }
    }
    /**
     遍历所有的结点,都进行广度优先搜索
     *
     */
    public void BFS(){
        for (int i = 0; i < vertexList.size(); i++) {
            if(!isVisited[i])BFS(i);
        }
    }

    /**
     * 广度优先遍历
     * @param vertex
     */
    private void bfs(int vertex){
        //未被访问，打印顶点信息
        if(!isVisited[vertex]){
            System.out.print(vertexList.get(vertex)+"->");
            isVisited[vertex]=true;
        }
        //继续访问相邻元素
        for (int i = vertex+1; i <vertexList.size() ; i++) {
            //如果存在且未被访问
            if(edges[vertex][i]>0&&!isVisited[i]){
                System.out.print(vertexList.get(i)+"->");
                isVisited[i]=true;
                queue.add(i);
            }
        }
        //相邻元素访问完了（广度优先），再让队列中的元素出队，继续访问
        while (!queue.isEmpty()){
            //队首元素出队，继续访问
            bfs(queue.remove());
        }
    }
    public void bfs(){
        for (int i = 0; i < vertexList.size(); i++) {
            if(!isVisited[i])bfs(i);
        }
    }
}
