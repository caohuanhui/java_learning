package pers.chh3213.create_graph;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.create_graph
 * @ClassName : Graph.java
 * @createTime : 2022/2/19 9:19
 * @Email :
 * @Description :
 */
public class Graph {
    public static void main(String[] args) {
        String[] vertex = {"A","B","C","D","E"};
        Graph graph = new Graph(vertex.length);
        //插入图的顶点
        for (int i = 0; i < vertex.length; i++) {
            graph.insertvertex(vertex[i]);
        }
        //指明连接的边
        graph.insertEdge(0,1,1);
        graph.insertEdge(0,2,1);
        graph.insertEdge(1,0,1);
        graph.insertEdge(1,2,1);
        graph.insertEdge(1,3,1);
        graph.insertEdge(1,4,1);
        graph.insertEdge(2,0,1);
        graph.insertEdge(2,1,1);
        graph.insertEdge(3,1,1);
        graph.insertEdge(4,1,1);
        //显示图的邻接矩阵
        graph.showGraph();
    }
    //存储顶点String
    ArrayList<String> vertexList;
    //保存矩阵
    int[][]edges;
    //边的数量
    int numOfEdges;
    //顶点总数;
    int numOfvertex;

    /**
     * 根据顶点总数进行初始化
     * @param numOfvertex
     */
    public Graph(int numOfvertex) {
        this.numOfvertex = numOfvertex;
        vertexList = new ArrayList<>(numOfvertex);
        edges = new int[numOfvertex][numOfvertex];
        numOfEdges=0;
    }

    /**
     * 插入顶点
     * @param vertex
     */
    public void insertvertex(String vertex){
        vertexList.add(vertex);
    }

    /**
     * 创建边
     * @param v1 表示点的下标即使第几个顶点 "A"-"B" "A"->0 "B"->1
     * @param v2 第二个顶点对应的下标
     * @param weight 表示0或1
     */
    public void insertEdge(int v1,int v2,int weight){
        edges[v1][v2]=weight;
        edges[v2][v1]=weight;
        numOfEdges++;
    }

    /**
     * 显示图
     */
    public void showGraph(){
        for (int[] list:edges) {
            System.out.println(Arrays.toString(list));
        }
    }
}
