package pers.chh3213.binary_tree.huffman_code.decode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman_code.decode
 * @ClassName : Node.java
 * @createTime : 2022/2/17 16:25
 * @Email :
 * @Description :
 */
public class Node implements Comparable<Node>{
    Byte data;//数据
    int value;//节点权值
    Node left;//左子节点
    Node right;//右子节点
    //存放霍夫曼编码
    private Map<Byte, String> codeMap = new HashMap<>();
    public Node(Byte data, int value) {
        this.data = data;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(Node o) {
        //表示从小到大排序
        return value-o.value;
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        System.out.println(this);
        if(left!=null){
            left.preTraverse();
        }
        if(right!=null){
            right.preTraverse();
        }
    }

    /**
     * 将传入的node结点的所有叶子结点的赫夫曼编码得到,并放入到huffmanCodes集合
     * @param node 传入的节点
     * @param code 路径：左子节点为0，右子节点为1
     * @param stringBuilder 用于拼接路径
     */
    private void getCodes(Node node, String code,StringBuilder stringBuilder){
        //新建拼接路径
        StringBuilder builder = new StringBuilder(stringBuilder);
        builder.append(code);
        if(node!=null){
            //判断当前node是叶子结点还是非叶子结点
            //如果是非叶子节点，就继续向下遍历
            if(node.data==null){
                //左递归
                getCodes(node.left,"0",builder);
                //右递归
                getCodes(node.right,"1",builder);
            }else{//说明是叶子节点
                codeMap.put(node.data,builder.toString());
            }
        }
    }

    public Map<Byte,String>getCodeMap(Node root){
        if(root==null)return null;
        //方式1
        getCodes(this, "", new StringBuilder());
        //方式2
        //getCodes(root.left,"0",new StringBuilder());
        //getCodes(root.right,"1",new StringBuilder());
        return codeMap;
    }

}
