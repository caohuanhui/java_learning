package pers.chh3213.binary_tree.huffman_code.decode;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman
 * @ClassName : HuffmanCode.java
 * @createTime : 2022/2/17 14:36
 * @Email :
 * @Description :霍夫曼编码
 */
public class HuffmanCode {

    public ArrayList<Node> getList(String codes) {
        //得到字符串对应的字节数组
        byte[] byteCodes = codes.getBytes();

        //创建哈希表，用于存放数据及其权值（出现次数）
        Map<Byte, Integer> dataAndWight = new HashMap<>();
        for(byte b : byteCodes) {
            Integer wight = dataAndWight.get(b);
            //如果还没有该数据，就创建并让其权值为1
            if(dataAndWight.get(b) == null) {
                dataAndWight.put(b, 1);
            }else {
                //如果已经有了该数据，就让其权值加一
                dataAndWight.put(b, wight+1);
            }
        }

        //创建List，用于返回
        ArrayList<Node> list = new ArrayList<>();
        //遍历哈希表，放入List集合中
        for(Map.Entry<Byte, Integer> entry : dataAndWight.entrySet()) {
            list.add(new Node(entry.getKey(), entry.getValue()));
        }
        return list;
    }
    /**
     * 创建霍夫曼树
     * @param nodes
     * @return
     */
    public Node createHuffmanTree(ArrayList<Node>nodes){
        //对集合中的元素进行排序
        Collections.sort(nodes);
        while (nodes.size()>1){
            //权值最小的两棵二叉树在集合中对应的下标
            int leftIndex = 0;
            int rightIndex = 1;
            //取出权值最小的两棵二叉树
            Node leftNode = nodes.get(leftIndex);
            Node rightNode = nodes.get(rightIndex);
            //构建新的二叉树,根节点没有data，只有权值
            Node parent = new Node(null,leftNode.value+rightNode.value);
            parent.left=leftNode;
            parent.right=rightNode;
            //从集合中移除处理过的两个最小的节点，并将父节点放入集合中
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            nodes.add(parent);
            //再次排序
            Collections.sort(nodes);
        }
        //返回根节点
        return nodes.get(0);
    }

}

