package pers.chh3213.binary_tree.huffman_code.decode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman_code.decode
 * @ClassName : Decode.java
 * @createTime : 2022/2/17 16:27
 * @Email :
 * @Description :
 */
public class Decode {
    /**
     * *将一个byte转成一个二进制的字符串,如果看不懂,可以参考我讲的Java基础二进制的原码,反码,补
     * 码
     * @param flag 标志是否需要补高位如果是true,表示需要补高位,
     *             如果是false表示不补,如果是最后一个字节,无需补高位
     * @param b 传入byte
     * @return 是该b对应的二进制的字符串, (注意是按补码返回)
     */
    public String byteToBitString(boolean flag, byte b){
        //使用变量保存b,将b转为int
        int temp = b;
        //如果是正数我们还存在补高位
        if(flag){
            //按位与256 1 0000 0000 | 0000 0001 => 1 0000 0001
            temp|=256;
        }
        //返回的是temp对应的二进制的补码
        String str = Integer.toBinaryString(temp);
        if(flag){
            return str.substring(str.length()-8);
        }else{
            return str;
        }
    }

    public byte[] deCode(Map<Byte,String>huffmanCodes, byte[]huffmanBytes){
        //1.先得到huffmanBytes对应的二进制的字符串，形式1010100010111，，，
        StringBuilder stringBuilder = new StringBuilder();
        //将byte数组转成二进制的字符串
        for (int i = 0; i < huffmanBytes.length; i++) {
            byte b = huffmanBytes[i];
            //判断是不是最后一个字节
            boolean flag = (i==huffmanBytes.length-1);
            stringBuilder.append(byteToBitString(!flag,b));
        }
        //把字符串安装指定的赫夫曼编码进行解码
        //把赫夫曼编码表进行调换,因为反向查询 a->100 100->a
        HashMap<String, Byte> map = new HashMap<>();
        for (Map.Entry<Byte,String>entry:huffmanCodes.entrySet()) {
            map.put(entry.getValue(), entry.getKey());
        }
        //创建集合，存放byte
        ArrayList<Byte> list = new ArrayList<>();
        for (int i = 0; i < stringBuilder.length(); ) {
            int count = 1;
            boolean flag = true;
            Byte b = null;
            while (flag){
                //递增地去除key
                //i不动，让count移动，指定匹配到一个字符
                String key = stringBuilder.substring(i, i + count);
                b = map.get(key);
                if(b==null){//说明没有匹配到
                    count++;
                }else{
                    flag = false;
                }
                list.add(b);
                //i直接移动到count
                i+=count;
            }
        }
        //当for循环结束后,我们list中就存放了所有的字符 "i like like like java do you like a java"
        //把list中的数据放入到byte[]并返回
        byte[] bytes = new byte[list.size()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i]=list.get(i);
        }
        return bytes;
    }
}
