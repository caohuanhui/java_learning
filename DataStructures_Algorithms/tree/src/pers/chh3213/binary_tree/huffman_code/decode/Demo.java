package pers.chh3213.binary_tree.huffman_code.decode;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman_code.decode
 * @ClassName : Demo.java
 * @createTime : 2022/2/17 16:45
 * @Email :
 * @Description :
 */
public class Demo {
    public static void main(String[] args) {
        String str = "i like like like java do you like a java";
        //String str = "helloworld";
        HuffmanCode huffmanTree = new HuffmanCode();
        ArrayList<Node> list = huffmanTree.getList(str);
        //构建霍夫曼树
        Node root = huffmanTree.createHuffmanTree(list);
        root.preTraverse();
        //进行霍夫曼编码
        Map<Byte, String> codeMap = root.getCodeMap(root);
        System.out.println(codeMap);

        //解码
        byte[] bytes = str.getBytes();
        Decode decode = new Decode();
        byte[] b = decode.deCode(codeMap,bytes);
        System.out.println(b);

    }
}
