package pers.chh3213.binary_tree.huffman_code;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman
 * @ClassName : HuffmanTree.java
 * @createTime : 2022/2/17 14:36
 * @Email :
 * @Description :霍夫曼编码
 */
public class HuffmanTree {
    public static void main(String[] args) {
        String str = "i like like like java do you like a java";
        //String str = "helloworld";
        HuffmanTree huffmanTree = new HuffmanTree();
        ArrayList<Node> list = huffmanTree.getList(str);
        //构建霍夫曼树
        Node root = huffmanTree.createHuffmanTree(list);
        root.preTraverse();
        //进行霍夫曼编码
        Map<Byte, String> codeMap = root.getCodeMap(root);
        System.out.println(codeMap);
    }
    public ArrayList<Node> getList(String codes) {
        //得到字符串对应的字节数组
        byte[] byteCodes = codes.getBytes();

        //创建哈希表，用于存放数据及其权值（出现次数）
        Map<Byte, Integer> dataAndWight = new HashMap<>();
        for(byte b : byteCodes) {
            Integer wight = dataAndWight.get(b);
            //如果还没有该数据，就创建并让其权值为1
            if(dataAndWight.get(b) == null) {
                dataAndWight.put(b, 1);
            }else {
                //如果已经有了该数据，就让其权值加一
                dataAndWight.put(b, wight+1);
            }
        }

        //创建List，用于返回
        ArrayList<Node> list = new ArrayList<>();
        //遍历哈希表，放入List集合中
        for(Map.Entry<Byte, Integer> entry : dataAndWight.entrySet()) {
            list.add(new Node(entry.getKey(), entry.getValue()));
        }
        return list;
    }
    /**
     * 创建霍夫曼树
     * @param nodes
     * @return
     */
    public Node createHuffmanTree(ArrayList<Node>nodes){
        //对集合中的元素进行排序
        Collections.sort(nodes);
        while (nodes.size()>1){
            //权值最小的两棵二叉树在集合中对应的下标
            int leftIndex = 0;
            int rightIndex = 1;
            //取出权值最小的两棵二叉树
            Node leftNode = nodes.get(leftIndex);
            Node rightNode = nodes.get(rightIndex);
            //构建新的二叉树,根节点没有data，只有权值
            Node parent = new Node(null,leftNode.value+rightNode.value);
            parent.left=leftNode;
            parent.right=rightNode;
            //从集合中移除处理过的两个最小的节点，并将父节点放入集合中
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            nodes.add(parent);
            //再次排序
            Collections.sort(nodes);
        }
        //返回根节点
        return nodes.get(0);
    }

}

class Node implements Comparable<Node>{
    Byte data;//数据
    int value;//节点权值
    Node left;//左子节点
    Node right;//右子节点
    //存放霍夫曼编码
    private Map<Byte, String> codeMap = new HashMap<>();
    public Node(Byte data, int value) {
        this.data = data;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(Node o) {
        //表示从小到大排序
        return value-o.value;
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        System.out.println(this);
        if(left!=null){
            left.preTraverse();
        }
        if(right!=null){
            right.preTraverse();
        }
    }

    /**
     * 将传入的node结点的所有叶子结点的赫夫曼编码得到,并放入到huffmanCodes集合
     * @param node 传入的节点
     * @param code 路径：左子节点为0，右子节点为1
     * @param stringBuilder 用于拼接路径
     */
    private void getCodes(Node node, String code,StringBuilder stringBuilder){
        //新建拼接路径
        StringBuilder builder = new StringBuilder(stringBuilder);
        builder.append(code);
        if(node!=null){
            //判断当前node是叶子结点还是非叶子结点
            //如果是非叶子节点，就继续向下遍历
            if(node.data==null){
                //左递归
                getCodes(node.left,"0",builder);
                //右递归
                getCodes(node.right,"1",builder);
            }else{//说明是叶子节点
                codeMap.put(node.data,builder.toString());
            }
        }
    }

    public Map<Byte,String>getCodeMap(Node root){
        if(root==null)return null;
        //方式1
        getCodes(this, "", new StringBuilder());
        //方式2
        //getCodes(root.left,"0",new StringBuilder());
        //getCodes(root.right,"1",new StringBuilder());
        return codeMap;
    }
}