package pers.chh3213.binary_tree.traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : BinaryTree.java
 * @createTime : 2022/2/16 13:49
 * @Email :
 * @Description :
 */
public class BinaryTree {
    //定义根节点
    private Node root;

    public void setRoot(Node root) {
        this.root = root;
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        if(this.root!=null){
            System.out.println("前序遍历");
            this.root.preTraverse();
        }else{
            System.out.println("二叉树为空");
        }
    }

    /**
     * 中序遍历
     */
    public void infixTraverse(){
        if(this.root!=null){
            System.out.println("中序遍历");
            this.root.infixTraverse();
        }else{
            System.out.println("二叉树为空");
        }
    }

    /**
     * 后序遍历
     */
    public void postTraverse(){
        if(this.root!=null){
            System.out.println("后序遍历");
            this.root.postTraverse();
        }else{
            System.out.println("二叉树为空");
        }
    }
}
