package pers.chh3213.binary_tree.traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : Node.java
 * @createTime : 2022/2/16 13:50
 * @Email :
 * @Description :创建节点
 */
public class Node {
    public int no;
    public String name;
    public Node left;
    public Node right;

    public Node(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        //先输出父节点
        System.out.println(this);
        //递归向左子树前序遍历
        if(this.left!=null){
            this.left.preTraverse();
        }
        //递归向右子树前序遍历
        if(this.right!=null){
            this.right.preTraverse();
        }
    }

    /**
     * 中序遍历
     */
    public void infixTraverse(){
        //递归向左子树前序遍历
        if(this.left!=null){
            this.left.infixTraverse();
        }
        //输出父节点
        System.out.println(this);
        //递归向右子树前序遍历
        if(this.right!=null){
            this.right.infixTraverse();
        }
    }

    public void postTraverse(){
        //递归向左子树前序遍历
        if(this.left!=null){
            this.left.postTraverse();
        }

        //递归向右子树前序遍历
        if(this.right!=null){
            this.right.postTraverse();
        }
        //输出父节点
        System.out.println(this);
    }
}
