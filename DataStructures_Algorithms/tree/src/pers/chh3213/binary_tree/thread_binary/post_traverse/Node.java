package pers.chh3213.binary_tree.thread_binary.post_traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : Node.java
 * @createTime : 2022/2/17 9:41
 * @Email :
 * @Description :创建节点
 */
public class Node {
    private int no;
    private String name;
    private Node left;
    private Node right;
    //父节点指针，为了后续线索化使用
    private Node parent;

    /*
    说明
    1.如果leftType==0表示指向的是左子树,如果1则表示指向前驱结点
    2. 如果rightType==0表示指向是右子树,如果1表示指向后继结点
    */
    private int leftType;
    private int rightType;

    public Node(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    public int getLeftType() {
        return leftType;
    }

    public void setLeftType(int leftType) {
        this.leftType = leftType;
    }

    public int getRightType() {
        return rightType;
    }

    public void setRightType(int rightType) {
        this.rightType = rightType;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }
    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

}
