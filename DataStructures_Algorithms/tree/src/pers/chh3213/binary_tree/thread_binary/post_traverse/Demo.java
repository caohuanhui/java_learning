package pers.chh3213.binary_tree.thread_binary.post_traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.thread_binary
 * @ClassName : Demo.java
 * @createTime : 2022/2/17 10:07
 * @Email :
 * @Description :
 */
public class Demo {
    public static void main(String[] args) {
        Node root = new Node(1, "a");
        Node node2 = new Node(3, "b");
        Node node3 = new Node(6, "c");
        Node node4 = new Node(8, "d");
        Node node5 = new Node(10, "e");
        Node node6 = new Node(14, "f");
        //手动创建二叉树
        ThreadedBinaryTree threadedBinaryTree = new ThreadedBinaryTree();
        root.setLeft(node2);
        root.setRight(node3);
        node2.setLeft(node4);
        node2.setRight(node5);
        node3.setLeft(node6);
        threadedBinaryTree.setRoot(root);
        

        //后序线索化
        threadedBinaryTree.postThreaded();

        //测试:以10号节点为例
        Node node5Left = node5.getLeft();
        Node node5Right = node5.getRight();
        System.out.println("10号节点的前驱节点是："+node5Left);//3
        System.out.println("10号节点的后继节点是："+node5Right);//1

        System.out.println("线索化方式遍历线索化二叉树");
        threadedBinaryTree.postThreadedTraverse();//8 10 3 14 6 1

    }
}
