package pers.chh3213.binary_tree.thread_binary.post_traverse;


/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.thread_binary
 * @ClassName : ThreadedBinaryTree.java
 * @createTime : 2022/2/17 9:49
 * @Email :
 * @Description :
 */
public class ThreadedBinaryTree {
    //定义根节点
    private Node root;

    //为了实现线索化,需要创建指向当前结点的前驱结点的指针
    //在递归进行线索化时,pre总是指向前一个结点
    private Node pre = null;

    public void setRoot(Node root) {
        this.root = root;
    }

    /**
     * 实现对二叉树进行后序线索化
     * @param node 当前需要线索化的节点
     */
    private void postThreaded(Node node){
        //如果当前节点为空，不能线索化
        if(node==null){
            return;
        }

        //1.线索化左子树
        if(node.getLeftType()==0){
            postThreaded(node.getLeft());
        }

        //3.线索化右子树
        if(node.getRightType()==0){
            postThreaded(node.getRight());
        }
        //2.线索化当前节点
        //如果当前节点的左指针为空，就指向前驱节点，并改变左指针类型
        if(node.getLeft()==null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        //通过前驱节点来将右指针的值令为后继节点
        if(pre!=null&&pre.getRight()==null){
            //让前驱结点的右指针指向当前结点
            pre.setRight(node);
            //修改右指针类型
            pre.setRightType(1);
        }
        //每处理一个结点,就令当前结点为下一个结点的前驱结点
        pre = node;

    }

    public void postThreaded(){
        this.postThreaded(root);
    }
    /**
     * 后序遍历线索化二叉树
     */
    public void postThreadedTraverse(){
        //定义一个变量,存储当前遍历的结点,从root开始
        Node node = root;
        //后序遍历开始节点是最左节点
        while (node!=null&&node.getLeftType()==0){
            node=node.getLeft();
        }
        Node preNode = null;
        while (node!=null){
            //如果右节点是线索
            if (node.getRightType()==1){
                //替换这个遍历的结点
                System.out.println(node);
                preNode = node;
                node = node.getRight();
            }else{
                //如果上个处理的节点是当前节点的右节点
                if(node.getRight() == preNode){
                    System.out.println(node);
                    if(node==root)return;
                    preNode = node;
                    node = node.getParent();
                }else{
                    //如果从左节点进入则找到右子树的最左节点
                    node = node.getRight();
                    while (node!=null&&node.getLeftType()==0){
                        node = node.getLeft();
                    }
                }
            }
        }

    }



}
