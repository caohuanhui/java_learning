package pers.chh3213.binary_tree.thread_binary.infix_traverse;


/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.thread_binary
 * @ClassName : ThreadedBinaryTree.java
 * @createTime : 2022/2/17 9:49
 * @Email :
 * @Description :
 */
public class ThreadedBinaryTree {
    //定义根节点
    private Node root;

    //为了实现线索化,需要创建指向当前结点的前驱结点的指针
    //在递归进行线索化时,pre总是指向前一个结点
    private Node pre = null;

    public void setRoot(Node root) {
        this.root = root;
    }

    /**
     * 实现对二叉树进行中序线索化
     * @param node 当前需要线索化的节点
     */
    private void infixThreaded(Node node){
        //如果当前节点为空，不能线索化
        if(node==null){
            return;
        }
        //1.线索化左子树
        infixThreaded(node.getLeft());
        //2.线索化当前节点
        //如果当前节点的左指针为空，就指向前驱节点，并改变左指针类型
        if(node.getLeft()==null){
            node.setLeft(pre);
            node.setLeftType(1);
        }
        //通过前驱节点来将右指针的值令为后继节点
        if(pre!=null&&pre.getRight()==null){
            //让前驱结点的右指针指向当前结点
            pre.setRight(node);
            //修改右指针类型
            pre.setRightType(1);
        }
        //每处理一个结点,就令当前结点为下一个结点的前驱结点
        pre = node;
        //3.线索化右子树
        infixThreaded(node.getRight());

    }

    public void infixThreaded(){
        this.infixThreaded(root);
    }
    /**
     * 中序遍历线索化二叉树
     */
    public void infixThreadedTraverse(){
        //定义一个变量,存储当前遍历的结点,从root开始
        Node node = root;
        //非递归的方法遍历,如果node不为空就一直循环
        while (node!=null){
            //循环遍历二叉树的左子树，直到某个节点的左子树指向前驱节点
            while (node.getLeftType()!=1){
                node=node.getLeft();
            }
            //打印当前节点
            System.out.println(node);
            //如果当前结点的右子树指向的是后继结点,就一直输出
            while (node.getRightType()==1){
                node = node.getRight();
                System.out.println(node);
            }
            //替换这个遍历的结点
            node = node. getRight();
        }

    }

    /**
     * 中序遍历
     */
    public void infixTraverse(){
        if(this.root!=null){
            System.out.println("中序遍历");
            this.root.infixTraverse();
        }else{
            System.out.println("二叉树为空");
        }
    }
}
