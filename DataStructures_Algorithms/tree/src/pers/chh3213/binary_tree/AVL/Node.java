package pers.chh3213.binary_tree.AVL;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.AVL
 * @ClassName : Node.java
 * @createTime : 2022/2/18 11:48
 * @Email :
 * @Description :
 */
public class Node {
     int value;
     Node left;
     Node right;

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    /**
     * 返回以该节点为根节点的树的高度
     * @return
     */
    public int height(){
        return Math.max(left==null?0:left.height(),right==null?0: right.height())+1;
    }

    /**
     * 获得左子树高度
     * @return
     */
    public int leftHeight(){
        if(left==null)return 0;
        return left.height();
    }
    
    /**
     * 返回右子树高度
     * @return
     */
    public int rightHeight(){
        if(right==null)return 0;
        return right.height();
    }
    /**
     * 左旋转
     */
    private void leftRotate(){
        //创建新的结点,以当前根结点的值
        Node newNode = new Node(this.value);
        //把新的结点的左子树设置成当前结点的左子树
        newNode.left=this.left;
        //新节点的右子树指向当前节点的右子树的左子树
        newNode.right=this.right.left;
        //把当前结点的值替换成当前节点的右子树的值
        this.value=this.right.value;
        //把当前结点的右子树设置成当前结点右子树的右子树
        this.right = this.right.right;
        //把当前结点的左子树(左子结点)设置成新的结点
        this.left = newNode;
    }

    /**
     * 右旋转
     */
    private void rightRotate(){
        //创建一个新节点，其值为当前节点的值
        Node newNode = new Node(this.value);
        //将新节点的右子树指向当前节点的右子树
        newNode.right = this.right;
        //将新节点的左子树指向当前节点的左子树的右子树
        newNode.left = this.left.right;
        //将当前节点的值改为其左子树的值
        this.value = this.left.value;
        //将当前节点的左子树指向其左子树的左子树
        this.left = this.left.left;
        //将当前节点的右子树指向新节点
        this.right=newNode;
    }
    
    /**
     * 添加节点到二叉排序树的对应位置
     * @param node
     */
    public void add(Node node){
        if(node==null){
            return;
        }
        if(value>node.value){
            if(this.left==null){
                this.left=node;
            }else{
                //左递归
                this.left.add(node);
            }
        }else if(value<=node.value){
            if(this.right==null){
                this.right=node;
            }else{
                //右递归
                this.right.add(node);
            }
        }
        //添加完一个节点后，如果:(右子树的高度-左子树的高度)>1,左旋转
        if(rightHeight()-leftHeight()>1){
            //如果当前节点右子树的左子树高度高于当前节点右子树的右子树高度
            if(this.right!=null&&right.leftHeight()>right.rightHeight()){
                //其右子树先进行右旋转
                right.rightRotate();
                //然后对当前节点进行左旋转
                this.leftRotate();
            }else{//直接进行左旋转
                leftRotate();
            }
        }
        //当添加完一个结点后,如果(左子树的高度-右子树的高度)>1,右旋转
        if(leftHeight()-rightHeight()>1){
            //如果它的左子树的右子树高度大于它的左子树的高度
            if(this.left!=null&&this.left.rightHeight()>this.left.leftHeight()){
                ///先对当前结点的左结点(左子树)->左旋转
                this.left.leftRotate();
                //再对当前结点进行右旋转
                this.rightRotate();
            }else{//否则直接进行右旋转
                this.rightRotate();
            }
        }
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        System.out.println(this);
        if(this.left!=null) this.left.preTraverse();
        if(this.right!=null)this.right.preTraverse();
    }
}
