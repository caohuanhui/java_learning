package pers.chh3213.binary_tree.AVL;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.AVL
 * @ClassName : Demo.java
 * @createTime : 2022/2/18 14:32
 * @Email :
 * @Description :
 */
public class Demo {
    public static void main(String[] args) {
        int[] arr = {10, 7, 11, 5, 8, 9};
        AVLTree avlTree = new AVLTree();
        for (int value: arr) {
            avlTree.addNode(new Node(value));
        }
        System.out.println("前序遍历");// 8 7 5 10 9 11
        avlTree.preTraverse();
        System.out.println("==========");
        System.out.println("树的高度："+avlTree.getRoot().height());
        System.out.println("树的左子树高度："+avlTree.getRoot().leftHeight());
        System.out.println("树的右子树高度："+avlTree.getRoot().rightHeight());
        System.out.println("左右子树高度差(左-右)："+avlTree.leftRightDiff());
    }
}
