package pers.chh3213.binary_tree.binary_sort.delete;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.binary_sort.delete
 * @ClassName : Node.java
 * @createTime : 2022/2/18 8:55
 * @Email :
 * @Description :
 */
public class Node {
    int value;
    Node left;
    Node right;

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    /**
     * 添加节点到二叉排序树的对应位置
     * @param node
     */
    public void add(Node node){
        if(node==null){
            return;
        }
        if(value>node.value){
            if(this.left==null){
                this.left=node;
            }else{
                //左递归
                this.left.add(node);
            }
        }else if(value<=node.value){
            if(this.right==null){
                this.right=node;
            }else{
                //右递归
                this.right.add(node);
            }
        }
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        System.out.println(this);
        if(this.left!=null) this.left.preTraverse();
        if(this.right!=null)this.right.preTraverse();
    }

    /**
     * 得到目标节点
     * @param targetValue
     * @return
     */
    public Node getTargetNode(int targetValue){
        //如果当前节点就是目标节点，就返回
        if(this.value==targetValue)return this;
        //如果当前节点的值大于目标节点，就向左查找，反之向右查找
        if(this.value>targetValue){
            if(left==null)return null;
            else{
                return left.getTargetNode(targetValue);
            }
        }else{
            if(right==null)return null;
            else{
                return right.getTargetNode(targetValue);
            }
        }
    }

    /**
     * 得到目标节点的父节点
      * @param targetValue 目标节点的值
     * @return
     */
    public Node getParentNode(int targetValue){
        //如果左右子树是目标节点，就返回该节点，否则继续向下查找
        if((left!=null&&left.value==targetValue)||(right!=null&&right.value==targetValue)){
            return this;
        }else{
            if(left!=null&&this.value>targetValue){
                return left.getParentNode(targetValue);
            }
            if(right!=null&&this.value<=targetValue){
                return right.getParentNode(targetValue);
            }
            //没有父节点（根节点）
            return null;
        }
    }
}
