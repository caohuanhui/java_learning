package pers.chh3213.binary_tree.binary_sort.delete;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.binary_sort.add_traverse
 * @ClassName : BinarySortTree.java
 * @createTime : 2022/2/18 8:54
 * @Email :
 * @Description :
 */
public class BinarySortTree {
    private Node root;

    /**
     * 添加节点
     * @param node
     */
    public void addNode(Node node){
        //如果根节点为空，就直接将该节点作为根节点
        if(root==null){
            root=node;
            return;
        }
        //否则就插入该节点到对应的位置
        root.add(node);
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        if(root==null){
            System.out.println("二叉树为空");
            return;
        }else{
            root.preTraverse();
        }
    }

    /**
     * 返回目标节点
     * @param targetValue
     * @return
     */
    public Node getTargetNode(int targetValue){
        if(root==null){
            System.out.println("二叉树为空");
            return null;
        }
        return root.getTargetNode(targetValue);
    }

    /**
     * 返回目标节点的父节点
     * @param targetValue
     * @return
     */
    public Node getParentNode(int targetValue){
        if(root==null){
            System.out.println("二叉树为空");
            return null;
        }
        return root.getParentNode(targetValue);

    }

    /**
     * 找到以node为根节点的二叉树的最小节点的值
     * @param node 作为根节点的节点
     * @return 值最小的节点的值
     */
    public int getMinValue(Node node){
        Node target=node;
        //循环的查找左子节点，就会找到最小值
        while (target.left!=null){
            target=target.left;
        }
        return target.value;
    }

    /**
     * 找到以node为根节点的二叉树的最大节点的值
     * @param node
     * @return
     */
    public int getMaxValue(Node node){
        Node target=node;
        //循环的查找右子节点，就会找到最大值
        while (target.right!=null){
            target=target.right;
        }
        return target.value;
    }
    /**
     * 删除节点
     * @param targetValue 待删除节点的值
     */
    public void delNode(int targetValue){
        if(root==null){
            System.out.println("二叉树为空");
            return;
        }
        //找到待删除的节点`targetNode`；
        Node targetNode = getTargetNode(targetValue);
        if(targetNode==null){
            return;
        }
        //如果只有一个节点,则直接删除根节点
        if(root.left==null&&root.right==null){
            root=null;
            return;
        }
        //找到`targetNode`的父节点`parent`;
        Node parent = getParentNode(targetValue);
        ////如果父节点为空（待删除节点为根节点）
        if(parent==null){
            int minValue = getMinValue(targetNode.right);
            delNode(minValue);
            targetNode.value=minValue;
            return;
        }
        //1.如果要删除的节点为叶子节点
        if(targetNode.left==null&&targetNode.right==null){
            if(parent.left!=null&&targetNode==parent.left)parent.left=null;
            else if(parent.right!=null&&targetNode==parent.right)parent.right=null;
        }
        ////2.删除只有一棵子树的节点
        ////2.1如果有右子节点
        else if(targetNode.left==null&&targetNode.right!=null){
            if(parent.left!=null&&targetNode.value==parent.left.value)parent.left=targetNode.right;
            if(parent.right!=null&&targetNode.value==parent.right.value)parent.right=targetNode.right;
        }
        //2.2如果有左子节点
        else if(targetNode.left!=null&&targetNode.right==null){
            if(parent.left!=null&&targetNode==parent.left)parent.left=targetNode.left;
            if(parent.right!=null&&targetNode==parent.right)parent.right=targetNode.left;
        }
        //3.如如果删除有两棵子树的节点
        else if(targetNode.left!=null&&targetNode.right!=null){
            //得到并删除待删除节点右子树中值最小的节点
            int minValue = getMinValue(targetNode.right);
            delNode(minValue);
            //将值最小的节点的值作为新的目标节点
            targetNode.value=minValue;
        }
    }
}
