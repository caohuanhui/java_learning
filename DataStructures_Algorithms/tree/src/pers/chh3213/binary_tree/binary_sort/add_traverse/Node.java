package pers.chh3213.binary_tree.binary_sort.add_traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.binary_sort.add_traverse
 * @ClassName : Node.java
 * @createTime : 2022/2/18 8:55
 * @Email :
 * @Description :
 */
public class Node {
    int value;
    Node left;
    Node right;

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    /**
     * 添加节点到二叉排序树的对应位置
     * @param node
     */
    public void add(Node node){
        if(node==null){
            return;
        }
        if(value>node.value){
            if(this.left==null){
                this.left=node;
            }else{
                //左递归
                this.left.add(node);
            }
        }else if(value<=node.value){
            if(this.right==null){
                this.right=node;
            }else{
                //右递归
                this.right.add(node);
            }
        }
    }

    /**
     * 前序遍历
     */
    public void preTraverse(){
        System.out.println(this);
        if(this.left!=null) this.left.preTraverse();
        if(this.right!=null)this.right.preTraverse();
    }
}
