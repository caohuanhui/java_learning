package pers.chh3213.binary_tree.binary_sort.add_traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.binary_sort.add_traverse
 * @ClassName : BinarySortTreeDemo.java
 * @createTime : 2022/2/18 9:04
 * @Email :
 * @Description :
 */
public class BinarySortTreeDemo {
    public static void main(String[] args) {
        int[] arr = {7, 3, 10, 12, 5, 1, 9};
        BinarySortTree binarySortTree = new BinarySortTree();
        for (int i = 0; i < arr.length; i++) {
            binarySortTree.addNode(new Node(arr[i]));
        }
        System.out.println("前序遍历");
        binarySortTree.preTraverse();// 7 3 1 5 10 9 12
    }
}
