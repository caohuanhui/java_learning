package pers.chh3213.binary_tree.binary_sort.add_traverse;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.binary_sort.add_traverse
 * @ClassName : BinarySortTree.java
 * @createTime : 2022/2/18 8:54
 * @Email :
 * @Description :
 */
public class BinarySortTree {
    private Node root;
    public void addNode(Node node){
        //如果根节点为空，就直接将该节点作为根节点
        if(root==null){
            root=node;
            return;
        }
        //否则就插入该节点到对应的位置
        root.add(node);
    }
    public void preTraverse(){
        if(root==null){
            System.out.println("二叉树为空");
            return;
        }else{
            root.preTraverse();
        }
    }

}
