package pers.chh3213.binary_tree.search;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : BinaryTree.java
 * @createTime : 2022/2/16 13:49
 * @Email :
 * @Description :
 */
public class BinaryTree {
    //定义根节点
    private Node root;

    public void setRoot(Node root) {
        this.root = root;
    }

    /**
     * 前序查找
     */
    public Node preSearch(int no){
        if(this.root!=null){
            return this.root.preSearch(no);
        }else{
            return null;
        }
    }

    /**
     * 中序查找
     */
    public Node infixSearch(int no){
        if(this.root!=null){
            return this.root.infixSearch(no);
        }else{
            return null;
        }
    }

    /**
     * 后序查找
     */
    public Node postSearch(int no){
        if(this.root!=null){
            return this.root.postSearch(no);
        }else{
            return null;
        }
    }
}
