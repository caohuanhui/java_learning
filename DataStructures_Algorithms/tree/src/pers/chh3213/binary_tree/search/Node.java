package pers.chh3213.binary_tree.search;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : Node.java
 * @createTime : 2022/2/16 13:50
 * @Email :
 * @Description :创建节点
 */
public class Node {
    public int no;
    public String name;
    public Node left;
    public Node right;

    public Node(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * 前序查找
     * @param no
     * @return
     */
    public Node preSearch(int no){
        if(this.no==no){
            return this;
        }
        //否则判断当前结点的左子节点是否为空,如果不为空,则递归前序查找
        //如果左递归前序查找,找到结点,则返回
        Node resNode = null;
        if(this.left!=null){
            resNode = this.left.preSearch(no);
        }
        //如果左子树找到
        if(resNode!=null){
            return resNode;
        }
        //右递归前序查找
        if(this.right!=null){
            resNode = this.right.preSearch(no);
        }
        return resNode;
    }
    /**
     * 中序查找
     */
    public Node infixSearch(int no){
        Node resNode = null;
        if(this.left!=null){
            resNode = this.left.infixSearch(no);
        }
        //如果找到，返回
        if(resNode!=null){
            return resNode;
        }
        if(this.no==no){
            return this;
        }
        if(this.right!=null){
            resNode = this.right.infixSearch(no);
        }
        return resNode;
    }
    /**
     * 后续查找
     */
    public Node postSearch(int no){
        Node resNode = null;
        if(this.left!=null){
            resNode = this.left.postSearch(no);
        }
        if(resNode!=null){
            return resNode;
        }
        if(this.right!=null){
            resNode = this.right.postSearch(no);
        }
        if(resNode!=null){
            return resNode;
        }
        if(this.no==no){
            return this;
        }
        return resNode;
    }
}
