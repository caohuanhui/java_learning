package pers.chh3213.binary_tree.delete;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : BinaryTreeDemo.java
 * @createTime : 2022/2/16 13:50
 * @Email :
 * @Description :
 */
public class BinaryTreeDemo {
    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        //手动创建节点，并放入二叉树中
        Node root = new Node(1, "aa");
        Node node2 = new Node(2, "bb");
        Node node3 = new Node(3, "cc");
        Node node4 = new Node(4, "dd");
        Node node5 = new Node(5, "ee");
        root.left=node2;
        root.right=node3;
        node3.left = node5;
        node3.right=node4;
        binaryTree.setRoot(root);

        binaryTree.preTraverse();
        binaryTree.delNode(4);
        binaryTree.preTraverse();

    }
}
