package pers.chh3213.binary_tree.delete;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : BinaryTree.java
 * @createTime : 2022/2/16 13:49
 * @Email :
 * @Description :
 */
public class BinaryTree {
    //定义根节点
    private Node root;

    public void setRoot(Node root) {
        this.root = root;
    }

    public void delNode(int no){
        if(root!=null){
            if(root.no==no){
                root=null;
            }else{
                root.delNode(no);
            }
        }else {
            System.out.println("空树");
        }
    }
    /**
     * 前序遍历
     */
    public void preTraverse(){
        if(this.root!=null){
            System.out.println("前序遍历");
            this.root.preTraverse();
        }else{
            System.out.println("二叉树为空");
        }
    }

}
