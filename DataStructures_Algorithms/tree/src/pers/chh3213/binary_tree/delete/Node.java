package pers.chh3213.binary_tree.delete;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree
 * @ClassName : Node.java
 * @createTime : 2022/2/16 13:50
 * @Email :
 * @Description :创建节点
 */
public class Node {
    public int no;
    public String name;
    public Node left;
    public Node right;

    public Node(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    public void delNode(int no){
        //如果当前结点的左子结点不为空，并且左子结点就是要删除结点
        if(this.left!=null&&this.left.no==no){
            this.left=null;
            return;
        }
        //如果当前结点的右子结点不为空，并且右子结点就是要删除结点
        if(this.right!=null&&this.right.no==no){
            this.right=null;
            return;
        }
        //向左子树进行递归删除
        if(this.left!=null){
            this.left.delNode(no);
        }
        //向右子树进行递归删除
        if(this.right!=null){
            this.right.delNode(no);
        }
    }
    public void preTraverse(){
        //先输出父节点
        System.out.println(this);
        //递归向左子树前序遍历
        if(this.left!=null){
            this.left.preTraverse();
        }
        //递归向右子树前序遍历
        if(this.right!=null){
            this.right.preTraverse();
        }
    }
}
