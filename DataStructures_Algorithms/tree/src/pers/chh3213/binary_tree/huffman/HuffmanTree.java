package pers.chh3213.binary_tree.huffman;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.huffman
 * @ClassName : HuffmanTree.java
 * @createTime : 2022/2/17 14:36
 * @Email :
 * @Description :
 */
public class HuffmanTree {
    public static void main(String[] args) {
        int[] arr = {13, 7, 8, 3, 29, 6, 1};
        HuffmanTree huffmanTree = new HuffmanTree();
        Node root = huffmanTree.createHuffmanTree(arr);
        root.preTraverse();
    }
    public Node createHuffmanTree(int[] arr){
        //创建数组用于存放Node
        ArrayList<Node> nodes = new ArrayList<>();
        for (int value:arr) {
            nodes.add(new Node(value));
        }
        //对集合中的元素进行排序
        Collections.sort(nodes);
        while (nodes.size()>1){
            //权值最小的两棵二叉树在集合中对应的下标
            int leftIndex = 0;
            int rightIndex = 1;
            //取出权值最小的两棵二叉树
            Node leftNode = nodes.get(leftIndex);
            Node rightNode = nodes.get(rightIndex);
            //构建新的二叉树
            Node parent = new Node(leftNode.value+rightNode.value);
            parent.left=leftNode;
            parent.right=rightNode;
            //从集合中移除处理过的两个最小的节点，并将父节点放入集合中
            nodes.remove(leftNode);
            nodes.remove(rightNode);
            nodes.add(parent);
            //再次排序
            Collections.sort(nodes);
        }
        //返回根节点
        return nodes.get(0);
    }

}

class Node implements Comparable<Node>{
    int value;//节点权值
    Node left;//左子节点
    Node right;//右子节点

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }

    @Override
    public int compareTo(Node o) {
        //表示从小到大排序
        return value-o.value;
    }
    public void preTraverse(){
        System.out.print(value+" ");
        if(left!=null){
            left.preTraverse();
        }
        if(right!=null){
            right.preTraverse();
        }
    }
}