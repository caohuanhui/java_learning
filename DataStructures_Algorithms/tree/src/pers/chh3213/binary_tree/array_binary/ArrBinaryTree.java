package pers.chh3213.binary_tree.array_binary;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.binary_tree.array_binary
 * @ClassName : ArrBinaryTree.java
 * @createTime : 2022/2/17 8:54
 * @Email :
 * @Description :实现顺序存储二叉树遍历
 */
public class ArrBinaryTree {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        //创建一个ArrBinaryTree
        ArrBinaryTree arrBinaryTree = new ArrBinaryTree(arr);
        System.out.println("前序");
        arrBinaryTree.preTraverse();//1 2 4 5 3 6 7
        System.out.println("中序");
        arrBinaryTree.infixTraverse();//4 2 5 1 6 3 7
        System.out.println("后序");
        arrBinaryTree.postTraverse();//4 5 2 6 7 3 1
    }
    //存储数据节点的数组
    private int[] arr;

    public ArrBinaryTree(int[] arr) {
        this.arr = arr;
    }

    /**
     * 重载
     */
    public void preTraverse(){
        this.preTraverse(0);
    }
    /**
     * 编写一个方法,完成顺序存储二叉树的前序遍历
     * @param index  数组下表
     */
    private void preTraverse(int index){
        //如果数组为空,或者arr.length=0
        if(arr==null||arr.length==0){
            System.out.println("数组为空，不能前序遍历");
            return;
        }
        //输出当前元素
        System.out.println(arr[index]);
        //想左递归遍历
        if((index*2+1)<arr.length){
            this.preTraverse(index*2+1);
        }
        //向右递归遍历
        if((index*2+2)<arr.length){
            this.preTraverse(index*2+2);
        }
    }

    public void infixTraverse(){
        this.infixTraverse(0);
    }
    /**
     * 中序遍历
     * @param index
     */
    private void infixTraverse(int index){
        //如果数组为空,或者arr.length=0
        if(arr==null||arr.length==0){
            System.out.println("数组为空，不能遍历");
            return;
        }
        if(2*index+1<arr.length){
            this.infixTraverse(2*index+1);
        }
        System.out.println(arr[index]);
        if(2*index+2<arr.length){
            this.infixTraverse(2*index+2);
        }
    }
    public void postTraverse(){
        this.postTraverse(0);
    }
    /**
     * 中序遍历
     * @param index
     */
    private void postTraverse(int index){
        //如果数组为空,或者arr.length=0
        if(arr==null||arr.length==0){
            System.out.println("数组为空，不能遍历");
            return;
        }
        if(2*index+1<arr.length){
            this.postTraverse(2*index+1);
        }
        if(2*index+2<arr.length){
            this.postTraverse(2*index+2);
        }
        System.out.println(arr[index]);
    }
}
