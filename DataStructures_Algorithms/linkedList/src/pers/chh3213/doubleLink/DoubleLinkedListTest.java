package pers.chh3213.doubleLink;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : PACKAGE_NAME
 * @ClassName : DoubleLinkedListTest.java
 * @createTime : 2022/1/29 20:18
 * @Email :
 * @Description :
 */
public class DoubleLinkedListTest {
    /**
     * 测试增删查改
     */
    @Test
    public void test01(){
        Node hero1 = new Node(1, "宋江","及时雨");
        Node hero2 = new Node(2,"卢俊义","玉麒麟");
        Node hero3 = new Node(3,"吴用","智多星");
        Node hero4 = new Node(4,"林冲","ef");
        Node hero5 = new Node(5, "宋","时雨");
        Node hero6 = new Node(6,"卢义","麒麟");
        Node hero7 = new Node(7,"用","多星");
        Node hero8 = new Node(8,"冲","豹子");

        DoubleLinkedList doubleLinkedList = new DoubleLinkedList();
        System.out.println("=======添加节点==========");
        //添加节点
        //doubleLinkedList.addLast(hero2);
        //doubleLinkedList.addLast(hero3);
        //doubleLinkedList.addLast(hero1);
        //doubleLinkedList.addLast(hero4);
        doubleLinkedList.addByOrder(hero1);
        doubleLinkedList.addByOrder(hero4);
        doubleLinkedList.addByOrder(hero3);
        doubleLinkedList.addByOrder(hero2);
        doubleLinkedList.showList();
        ////修改节点
        //doubleLinkedList.modifyNode(new Node(1, "宋","时雨"));
        //doubleLinkedList.showList();
        //删除节点
        System.out.println("-----------删除节点---------------");
        doubleLinkedList.delNode(3);
        doubleLinkedList.showList();
    }
}
