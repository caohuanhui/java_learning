package pers.chh3213.doubleLink;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : PACKAGE_NAME
 * @ClassName : DoubleLinkedList.java
 * @createTime : 2022/1/29 19:53
 * @Email :
 * @Description :
 */
public class DoubleLinkedList {
    //头节点,没有具体数据
    private Node head = new Node();

    /**
     * 返回头节点
     * @return
     */
    public Node getHead() {
        return head;
    }

    /**
     * 显示链表信息
     */
    public void showList(){
        Node temp = head;
        if(temp.next==null){
            System.out.println("链表为空");
            return;
        }
        while(temp!=null){
            System.out.println(temp);
            temp = temp.next;
        }
    }

    /**
     * 直接在链表尾部添加节点
     * @param newNode 待添加的节点
     */
    public void addLast(Node newNode){
        Node temp = head;
        //遍历链表
        while(temp.next!=null){
            temp=temp.next;
        }
        //当退出while循环时, temp就指向了链表的最后
        ///将最后这个节点的next指向新的节点,新节点的pre指向最后这个节点
        temp.next=newNode;
        newNode.pre = temp;

    }

    /**
     * 根据排名顺序插入节点，有空指针异常，尚未解决
     * @param newNode
     */
    public void addByOrder(Node newNode){
        Node temp = head;
        // 标志链表中是否已存在指定的编号，如果已存在，则不再添加节点
        boolean flag=false;

        while(temp.next!=null){
            if(temp.next.rank> newNode.rank){
                break;
            }else if(temp.next.rank== newNode.rank){//节点已存在
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            System.out.printf("节点%d已存在，不再添加",newNode.rank);
        }else{
            //System.out.println("========");
            //TODO:空指针异常
            newNode.next = temp.next;
            temp.next.pre = newNode;
            temp.next=newNode;
            newNode.pre=temp;
        }
    }

    /**
     * 修改节点
     * @param modNode
     */
    public void modifyNode(Node modNode){
        Node temp = head;
        while(temp.next!=null){
            temp=temp.next;
            //根据排名找到修改的节点
            if(temp.rank==modNode.rank){
                temp.name = modNode.name;
                temp.nickName = modNode.nickName;
                return;
            }
        }
        System.out.println("未找到该节点");
    }

    /**
     * 删除指定排名的节点
     * @param rank
     */
    public void delNode(int rank){
        Node temp = head.next;
        while(temp!=null){
            if(temp.rank==rank){
                temp.pre.next=temp.next;
                //如果是最后一个节点,就不需要执行下面这句话,否则出现空指针
                if(temp.next!=null){
                    temp.next.pre = temp.pre;
                }
                return;
            }
            temp=temp.next;
        }
        System.out.println("未找到所要删除的节点");
    }
}
