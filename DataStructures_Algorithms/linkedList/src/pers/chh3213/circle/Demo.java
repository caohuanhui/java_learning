package pers.chh3213.circle;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.circle
 * @ClassName : Demo.java
 * @createTime : 2022/1/30 13:55
 * @Email :
 * @Description :
 */
public class Demo {
    @Test
    public void test(){
        CircleSingleLinkedList circleSingleLinkedList = new CircleSingleLinkedList();
        circleSingleLinkedList.add(5);
        circleSingleLinkedList.showList();
        System.out.println("=============");
        //测试小孩出圈是否正确
        circleSingleLinkedList.countOut(5,1,2);
    }
}
