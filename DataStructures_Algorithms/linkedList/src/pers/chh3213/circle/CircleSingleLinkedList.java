package pers.chh3213.circle;

import java.nio.file.FileStore;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.circle
 * @ClassName : CircleSingleLinkedList.java
 * @createTime : 2022/1/30 13:33
 * @Email :
 * @Description :
 */
public class CircleSingleLinkedList {
    private Node first = null;

    /**
     * 添加节点，构建有nums个节点的环形链表
     */
    public void add(int nums){
        if(nums<1){
            System.out.println("输入的节点个数不正确");
            return;
        }
        //创建一个辅助指针(变量) cur , 事先应该指向环形链表的最后这个节点.
        Node cur = null;
        for(int i = 1; i <= nums; i++){
           //根据编号,创建节点
            Node node = new Node(i);
            //如果是第一个节点
            if(i==1){
                first = node;
                //构成环
                first.next=first;
                //让辅助指针指向第一个节点
                cur = first;
            }else{
                cur.next=node;
                node.next=first;
                cur = node;
            }
        }
    }

    /**
     * 遍历
     */
    public void showList(){
        //判断链表是否为空
        if(first==null){
            System.out.println("空链表");
            return;
        }
        //因为first不能动,因此我们仍然使用一个辅助指针完成遍历
        Node cur = first;
        while(cur.next!=first){
            System.out.println(cur);
            cur = cur.next;
        }
        // 最后一个节点
        System.out.println(cur);
    }

    /**
     * 计算小孩出圈顺序
     * @param n 起初有n个人
     * @param k 从第k个人开始报数
     * @param m 数m下
     */
    public void countOut(int n, int k,int m){
        //对输入数据校验
        if(first==null||k<1||k>n||n<1){
            System.out.println("输入参数有误");
            return;
        }
        //创建辅助指针,帮助完成小孩出圈
        Node cur = first;
        //cur,事先应该指向环形链表的最后一个节点
        while(cur.next!=first){
            cur =cur.next;
        }
        //小孩报数前,先让first和cur移动k-1次
        for (int i = 1; i < k; i++) {
            first = first.next;
            cur = cur.next;
        }
        //当小孩报数时, 让first和cur指针同时的移动 m-1次,然后出圈
        //这里是一个循环操作,直到圈中只有一个节点
        while(true){
            if(cur==first){
                break;
            }
            for (int i = 1; i <m ; i++) {
                first = first.next;
                cur = cur.next;
            }
            //这时first指向的节点,就是要出圈的小孩节点
            System.out.println("出圈序号："+first.getNo());
            ///这时将first指向的小孩节点出圈
            first = first.next;
            cur.next=first;
        }
        System.out.println("最后剩下的序号："+first.getNo());
    }
}
