package pers.chh3213.single;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.single
 * @ClassName : SingleLinkedListTest.java
 * @createTime : 2022/1/28 10:24
 * @Email :
 * @Description :
 */
public class SingleLinkedListTest {
    public static void main(String[] args) {
        //创建节点
        Node hero1 = new Node(1, "宋江","及时雨");
        Node hero2 = new Node(2,"卢俊义","玉麒麟");
        Node hero3 = new Node(3,"吴用","智多星");
        Node hero4 = new Node(4,"林冲","豹子头");
        // 创建链表
        SingleLinkedList singleLinkedList = new SingleLinkedList();
        //添加节点
        //singleLinkedList.addLast(hero2);
        //singleLinkedList.addLast(hero3);
        //singleLinkedList.addLast(hero1);
        //singleLinkedList.addLast(hero4);
        singleLinkedList.addByOrder(hero1);
        singleLinkedList.addByOrder(hero4);
        singleLinkedList.addByOrder(hero3);
        singleLinkedList.addByOrder(hero2);
        //显示链表
        singleLinkedList.showList();
        System.out.println("=========================");
        //修改节点
        singleLinkedList.modifyNode(new Node(2,"卢","玉麒麟"));
        singleLinkedList.showList();
        System.out.println("--------------------------");
        //删除节点
        singleLinkedList.delNode(2);
        singleLinkedList.showList();
        System.out.println("-----------------");
        System.out.println("有效节点个数:"+singleLinkedList.getLength());
        System.out.println("***************");
        System.out.println("倒数第2个节点："+singleLinkedList.findNode(1));
        ////反转链表
        //singleLinkedList.reverseList();
        //System.out.println("====================");
        //singleLinkedList.showList();
        singleLinkedList.reversePrint();
    }

    /**
     * 测试链表合并
     */
    @Test
    public void test(){
        //创建节点
        Node hero1 = new Node(1, "宋江","及时雨");
        Node hero2 = new Node(2,"卢俊义","玉麒麟");
        Node hero3 = new Node(3,"吴用","智多星");
        Node hero4 = new Node(4,"林冲","ef");
        Node hero5 = new Node(5, "宋","时雨");
        Node hero6 = new Node(6,"卢义","麒麟");
        Node hero7 = new Node(7,"用","多星");
        Node hero8 = new Node(8,"冲","豹子");
        // 创建链表
        SingleLinkedList singleLinkedList = new SingleLinkedList();
        SingleLinkedList singleLinkedList2 = new SingleLinkedList();
        SingleLinkedList singleLinkedList3 = new SingleLinkedList();
        //添加节点
        singleLinkedList.addByOrder(hero1);
        singleLinkedList.addByOrder(hero4);
        singleLinkedList.addByOrder(hero3);
        singleLinkedList.addByOrder(hero2);
        singleLinkedList2.addByOrder(hero5);
        singleLinkedList2.addByOrder(hero6);
        singleLinkedList2.addByOrder(hero7);
        singleLinkedList2.addByOrder(hero8);
        //显示链表
        singleLinkedList.showList();
        System.out.println("================");
        singleLinkedList2.showList();
        //合并
        System.out.println("**********************");
        twoLinkedList(singleLinkedList.getHead(),singleLinkedList2.getHead(),singleLinkedList3.getHead());
        singleLinkedList3.showList();
    }
    /**
     * 合并两个单链表
     *  传入待合并的两个链表的头节点以及第三个单链表的头节点
     * @param head1
     * @param head2
     * @param head3
     */
    public void twoLinkedList(Node head1, Node head2, Node head3){
        // 如果两个链表均为空，则无需合并，直接返回
        if(head1.next==null&&head2.next==null){
            return;
        }
        // 如果链表1为空，则将head3.next指向head2.next，实现链表2中的节点连接到链表3
        if(head1.next==null){
            head3.next = head2.next;
        }else{
            // 将head3.next指向head1.next，实现链表1中的节点连接到链表3
            head3.next=head1.next;
            // 定义一个辅助的指针（变量），帮助我们遍历链表2
            Node cur2 = head2.next;
            // 定义一个辅助的指针（变量），帮助我们遍历链表3
            Node cur3 = head3;
            Node next =null;
            // 遍历链表2，将其节点按顺序连接至链表3
            while (cur2!=null){
                // 链表3遍历完毕后，可以直接将链表2剩下的节点连接至链表3的末尾
                if(cur3.next==null){
                    cur3.next=cur2;
                    break;
                }
                // 在链表3中，找到第一个大于链表2中的节点编号的节点
                // 因为是单链表，找到的节点是位于添加位置的前一个节点，否则无法插入
                if(cur2.rank<= cur3.next.rank){
                    // 先暂时保存链表2中当前节点的下一个节点，方便后续使用
                    next = cur2.next;
                    // 将cur2的下一个节点指向cur3的下一个节点
                    cur3.next = cur2.next;
                    // 将cur2连接到链表3上
                    cur3.next = cur2;
                    cur2 = next;
                }
                cur3 = cur3.next;
            }
        }

    }
}
