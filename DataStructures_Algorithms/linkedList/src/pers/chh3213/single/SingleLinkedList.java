package pers.chh3213.single;

import java.nio.charset.StandardCharsets;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.single
 * @ClassName : SingleLinkedList.java
 * @createTime : 2022/1/28 9:09
 * @Email :
 * @Description :单链表实现水浒英雄管理，一个英雄的data包括：排名,名字，绰号
 */
public class SingleLinkedList {
    //头节点,没有具体数据
    private Node head = new Node();

    /**
     * 返回头节点
     * @return
     */
    public Node getHead() {
        return head;
    }

    /**
     * 直接在链表尾部添加节点
     * @param newNode 待添加的节点
     */
    public void addLast(Node newNode){
        Node temp = head;
        //遍历链表
        while(temp.next!=null){
            temp=temp.next;
        }
        //当退出while循环时, temp就指向了链表的最后
        ///将最后这个节点的next指向新的节点
        temp.next=newNode;
    }

    /**
     * 根据排名顺序插入节点
     * @param newNode
     */
    public void addByOrder(Node newNode){
        Node temp = head;
        // 标志链表中是否已存在指定的编号，如果已存在，则不再添加节点
        boolean flag=false;

        while(temp.next!=null){
            //temp是位于添加位置的前一个节点,否则插入不了
            if(temp.next.rank> newNode.rank){
                break;
            }else if(temp.next.rank== newNode.rank){//节点已存在
                flag=true;
                break;
            }
            temp=temp.next;
        }
        if(flag){
            System.out.printf("节点%d已存在，不再添加",newNode.rank);
        }else{
            newNode.next = temp.next;
            temp.next = newNode;
        }
    }

    /**
     * 修改节点
     * @param modNode
     */
    public void modifyNode(Node modNode){
        Node temp = head;
        while(temp.next!=null){
            temp=temp.next;
            //根据排名找到修改的节点
            if(temp.rank==modNode.rank){
                temp.name = modNode.name;
                temp.nickName = modNode.nickName;
                return;
            }
        }
        System.out.println("未找到该节点");
    }

    /**
     * 删除指定排名的节点
     * @param rank
     */
    public void delNode(int rank){
        Node temp = head;
        while(temp.next!=null){
            if(temp.next.rank==rank){
                temp.next=temp.next.next;
                return;
            }
            temp=temp.next;
        }
        System.out.println("未找到所要删除的节点");
    }

    /**
     * 显示链表信息
     */
    public void showList(){
        Node temp = head;
        if(temp.next==null){
            System.out.println("链表为空");
            return;
        }
        while(temp!=null){
            System.out.println(temp);
            temp = temp.next;
        }
    }

    /**
     * 获取有效节点个数，不包括头节点
     * @return
     */
    public int getLength(){
        Node temp=head.next;
        int len =0;
        while(temp!=null){
            temp = temp.next;
            len++;
        }
        return len;
    }

    /**
     *  查找单链表中的倒数第k个结点
     * @param k 倒数第k个结点
     * @return 查找到的节点
     */
    public Node findNode(int k){
        //倒数第k个结点即为正数n-k个节点
        Node temp = head;
        for(int i=0;i<getLength()+1-k;i++){
            temp = temp.next;
        }
        return temp;
    }

    /**
     * 反转链表
     */
    public void reverseList(){
        //如果当前链表为空,或者只有一个节点,无需反转,直接返回
        if(head.next==null||head.next.next==null){
            return;
        }
        Node temp = head.next;
        // 指向当前节点的下一个节点
        Node next = null;
        //定义新的头节点作为反转后链表的头节点
        Node reverseHead = new Node();
        //从头到尾遍历原来的链表,每遍历一个节点,就将其取出,并放在新的链表`reverseHead`的最前端
        while(temp!=null){
            //暂时保存当前节点的下一节点
            next = temp.next;
            //将temp的下一节点指向新的链表的最前端
            temp.next = reverseHead.next;
            //将temp连接到新的链表上
            reverseHead.next = temp;
            //将temp连接到新的链表上
            temp = next;
        }
        //将head.next指向reverseHead.next,实现单链表的反转
        head.next = reverseHead.next;

    }
    /**
     * 利用栈这个数据结构,将各个节点压入到栈中,
     * 然后利用栈的先进后出的特点,就实现了逆序打印的效果.**
     */
    public void reversePrint(){
        if(head.next==null){
            System.out.println("链表为空");
            return;
        }
        //创建一个栈，将各个节点压入栈
        Stack<Node> stack = new Stack<>();
        Node cur = head.next;
        //将链表所有的节点压入栈
        while(cur!=null){
            stack.push(cur);
            cur = cur.next;
        }
        //将栈中的节点pop出栈，并打印
        while (stack.size()>0){
            System.out.println(stack.pop());

        }
    }
}
