package pers.chh3213.single;

/**
 * Created with IntelliJ IDEA.
 *
 * @author : chh3213
 * @version : 1.0
 * @Project : DataStructures_Algorithms
 * @Package : pers.chh3213.single
 * @ClassName : Node.java
 * @createTime : 2022/1/28 9:11
 * @Email :
 * @Description :定义英雄节点，一个英雄的data包括：排名,名字，绰号
 */
public class Node {
    public int rank;
    public String name;
    public String nickName;
    //指向下一节点
    public Node next;

    /**
     * 构造器
     * @param rank
     * @param name
     * @param nickName
     */
    public Node(int rank, String name, String nickName) {
        this.rank = rank;
        this.name = name;
        this.nickName = nickName;
    }

    public Node() {
    }

    /**
     * 显示英雄信息
     * @return
     */
    @Override
    public String toString() {
        return "Node{" +
                "rank=" + rank +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                '}';
    }
}
