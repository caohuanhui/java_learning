package pers.chh3213.sort;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 *
* BucketSort.java
* @Description 桶排序
* @author chh3213
* @version
* @date 2022年2月15日下午2:20:23
 */
public class BucketSort {
	//使用快速排序对每个桶进行排序
	QuickSort quickSort = new QuickSort();
	public static void main(String[] args) {
		BucketSort bucket = new BucketSort();
		int[] arr = {9, 160, -31, 23, -30, 49, 25, 21, 30};
		arr = bucket.bucketSort(arr, 5);
		System.out.println(Arrays.toString(arr));
	}

	/**
	 * 桶排序
	 * @param arr 待排序数组
	 * @param bucketSize 桶的大小（容量）
	 * @return
	 */
	public int[] bucketSort(int[] arr, int bucketSize) {
		if(arr.length==0)return arr;
		//获取最大、最小值
		int maxValue = arr[0];
		int minValue = arr[0];
		for (int i : arr) {
			if(i<minValue)minValue=i;
			if(i>maxValue)maxValue=i;
		}

		//桶的数量
		int bucketCount = (int)Math.floor((maxValue-minValue)/bucketSize)+1;

		//根据取值范围"创建"对应数量的"桶"
		int[][] buckets = new int[bucketCount][0];

		// 利用映射函数将数据分配到各个桶中
		for (int i = 0; i < arr.length; i++) {
			int index = (int)Math.floor(arr[i]-minValue)/bucketSize;
			buckets[index]=arrAppend(buckets[index], arr[i]);
		}

		int arrIndex = 0;
		for (int[] bucket : buckets) {
			if(bucket.length<=0)continue;
			//对每个桶进行排序，这里使用了快速排序
			quickSort.quickSort(bucket, 0, bucket.length-1);
			for (int value : bucket) {
				arr[arrIndex++]=value;
			}
		}
		return arr;
	}
	/**
	 * 自动扩容，并保存数据
	 * @param arr
	 * @param value
	 * @return
	 */
	private int[] arrAppend(int[] arr, int value) {
		arr = Arrays.copyOf(arr, arr.length+1);
		arr[arr.length-1] = value;
		return arr;
	}
}
