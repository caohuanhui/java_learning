package pers.chh3213.sort;

import java.util.Arrays;
import java.util.Iterator;

/**
 *
* CountingSort.java
* @Description 计数排序
* @author chh3213
* @version
* @date 2022年2月15日上午10:46:13
 */
public class CountingSort{
	public static void main(String[] args) {
		CountingSort cSort = new CountingSort();
		int[] arr = {9, 16, 31, 23, 30, 49, 25, 21, 30};
		int[] result = cSort.countingSort(arr);
		System.out.println(Arrays.toString(result));
	}
	public int[] countingSort(int[] arr) {
		int maxValue = getMaxValue(arr);
		// 创建一个新数组，长度为max+1
		int countLen = maxValue+1;
		int[] count = new int[countLen];
		//统计数组中每个值为i的元素出现的次数，存入数组的第i项
		for (int i : arr) {
			count[i]++;
		}
		int[] result = new int[countLen];
		// 创建结果数组的起始索引
		int sortedIndex = 0;
		 // 遍历计数数组，将计数数组的索引填充到结果数组中
		for (int i = 0; i < countLen; i++) {
			while(count[i]>0) {
				result[sortedIndex++]=i;
				count[i]--;
			}
		}
		return result;
	}

	/**
	 * 找出待排序的数组中最大的元素
	 * @param arr 待排序的数组
	 * @return 返回最大值
	 */
	public int getMaxValue(int[] arr) {
		int maxValue = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if(maxValue<arr[i]) {
				maxValue=arr[i];
			}
		}
		return maxValue;
	}


}
