package pers.chh3213.sort;

import java.util.Arrays;

public class HeapSort {

	public static void main(String[] args) {
		HeapSort hSort = new HeapSort();
		int[] arr = {9, 160, -31, 25, -30, 49, 25, 21, 30};
		hSort.heapSort(arr);
		System.out.println(Arrays.toString(arr));
	}
	public int[] heapSort(int[] arr) {
		buildMaxHeap(arr);
		int len = arr.length;
		for (int i = len - 1; i > 0; i--) {
			swap(arr, 0, i);
			len--;
			heapify(arr, 0, len);
		}
		return arr;
	}
	private void buildMaxHeap(int[] arr) {
		int len = arr.length;
		for (int i = (int) Math.floor(len / 2); i >= 0; i--) {
			heapify(arr, i, len);
		}
	}

	private void heapify(int[] arr, int i, int len) {
		int left = 2*i+1;
		int right = 2*i+2;
		int largest = i;

		if(left<len&&arr[left]>arr[largest])largest=left;
		if(right<len&&arr[right]>arr[largest])largest=right;
		if(largest!=i) {
			swap(arr, i, largest);
			heapify(arr, largest, len);
		}
	}
	/**
	 * 交换元素
	 * @param arr
	 * @param i
	 * @param j
	 */
    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
