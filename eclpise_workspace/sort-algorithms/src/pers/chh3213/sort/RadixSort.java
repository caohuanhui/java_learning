package pers.chh3213.sort;

import java.util.Arrays;
/**
 *
* RadixSort.java
* @Description 基数排序
* @author chh3213
* @version
* @date 2022年2月15日下午3:51:22
 */
public class RadixSort {

	public static void main(String[] args) {
		RadixSort radix = new RadixSort();
		int[] arr = {9, 160, -31, 25, -30, 49, 25, 21, 30};
		radix.radixSort(arr);
		System.out.println(Arrays.toString(arr));
	}
	/**
	 * 获取数字位数
	 * @param num
	 * @return
	 */
    protected int getNumLenght(long num) {
//        if (num == 0) {
//            return 1;
//        }
//        int lenght = 0;
//        for (long temp = num; temp != 0; temp /= 10) {
//            lenght++;
//        }
//        return lenght;
      //将最大值转为字符串，它的长度就是它的位数
        int digits = (num + "").length();
        return digits;
    }
	/**
	 * 获取最大数的位数
	 * @param arr
	 * @return
	 */
	private int getMaxDigit(int[] arr) {
		int max = arr[0];
		for (int i : arr) {
			if(max<i)max=i;
		}
		return getNumLenght(max);
	}

	public int[] radixSort(int[] arr) {
		int mod = 10;
		int dev = 1;
		int maxDigit = getMaxDigit(arr);
		for (int i = 0; i < maxDigit; i++) {
			 // 考虑负数的情况，这里扩展一倍队列数，其中 [0-9]对应负数，[10-19]对应正数 (bucket + 10)
			int[][]counter = new int[mod*2][0];
			for (int j = 0; j < arr.length; j++) {
				int bucket = ((arr[j]%mod)/dev)+mod;
				counter[bucket]=arrayAppend(counter[bucket], arr[j]);
			}
			int pos = 0;
			for (int[] bucket : counter) {
				for (int value : bucket) {
					arr[pos++]=value;
				}
			}
			dev*=10;
			mod*=10;
		}
		return arr;
	}
    /**
     * 自动扩容，并保存数据
     *
     * @param arr
     * @param value
     */
    private int[] arrayAppend(int[] arr, int value) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = value;
        return arr;
    }
}
