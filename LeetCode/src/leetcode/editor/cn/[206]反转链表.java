//给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
// 
// 
// 
//
// 示例 1： 
//
// 
//输入：head = [1,2,3,4,5]
//输出：[5,4,3,2,1]
// 
//
// 示例 2： 
//
// 
//输入：head = [1,2]
//输出：[2,1]
// 
//
// 示例 3： 
//
// 
//输入：head = []
//输出：[]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目范围是 [0, 5000] 
// -5000 <= ListNode.val <= 5000 
// 
//
// 
//
// 进阶：链表可以选用迭代或递归方式完成反转。你能否用两种方法解决这道题？ 
// 
// 
// Related Topics 递归 链表 👍 2271 👎 0


//leetcode submit region begin(Prohibit modification and deletion)

import java.util.List;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode reverseList(ListNode head) {
        ////1. 头插法
        ////如果当前链表为空,或者只有一个节点,无需反转,直接返回
        //if(head==null||head.next==null){
        //    return head;
        //}
        //ListNode temp = head;
        //// 指向当前节点的下一个节点
        //ListNode next = null;
        ////定义新的头节点作为反转后链表的头节点
        //ListNode reverseHead = new ListNode();
        ////从头到尾遍历原来的链表,每遍历一个节点,就将其取出,并放在新的链表`reverseHead`的最前端
        //while(temp!=null){
        //    //暂时保存当前节点的下一节点
        //    next = temp.next;
        //    //将temp的下一节点指向新的链表的最前端
        //    temp.next = reverseHead.next;
        //    //将temp连接到新的链表上
        //    reverseHead.next = temp;
        //    //将temp后移
        //    temp = next;
        //}
        ////head.next = reverseHead.next;
        //return reverseHead.next;

        //2. 递归
        if(head==null||head.next==null){
            return head;
        }
        ListNode temp = head.next;
        ListNode reverseHead = reverseList(temp);
        temp.next = head;
        head.next = null;
        return reverseHead;

    }
}
//leetcode submit region end(Prohibit modification and deletion)
