//给你一个头结点为 head 的单链表和一个整数 k ，请你设计一个算法将链表分隔为 k 个连续的部分。 
//
// 每部分的长度应该尽可能的相等：任意两部分的长度差距不能超过 1 。这可能会导致有些部分为 null 。 
//
// 这 k 个部分应该按照在链表中出现的顺序排列，并且排在前面的部分的长度应该大于或等于排在后面的长度。 
//
// 返回一个由上述 k 部分组成的数组。 
// 
//
// 示例 1： 
//
// 
//输入：head = [1,2,3], k = 5
//输出：[[1],[2],[3],[],[]]
//解释：
//第一个元素 output[0] 为 output[0].val = 1 ，output[0].next = null 。
//最后一个元素 output[4] 为 null ，但它作为 ListNode 的字符串表示是 [] 。
// 
//
// 示例 2： 
//
// 
//输入：head = [1,2,3,4,5,6,7,8,9,10], k = 3
//输出：[[1,2,3,4],[5,6,7],[8,9,10]]
//解释：
//输入被分成了几个连续的部分，并且每部分的长度相差不超过 1 。前面部分的长度大于等于后面部分的长度。
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目在范围 [0, 1000] 
// 0 <= Node.val <= 1000 
// 1 <= k <= 50 
// 
// Related Topics 链表 👍 249 👎 0


//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode[] splitListToParts(ListNode head, int k) {
        //先得到链表长度
        int len=0;
        ListNode cur = head;
        while(cur!=null){
            len++;
            cur = cur.next;
        }
        int size = len/k;
        int mod = len%k;
        //k部分
        ListNode[] ret = new ListNode[k];
        cur = head;
        for (int i = 0; cur!=null&&i < k; i++) {
            ret[i]=cur;
            //前 mod 个部分的长度各为 size+1，其余每个部分的长度各为 size。
            int curSize=size+(mod-- >0?1:0);
            //将 curr 向后移动 curSize 步，则 curr 为当前部分的尾结点；
            for (int j = 0; j < curSize-1; j++) {
                cur = cur.next;
            }
            //当 curr 到达当前部分的尾结点时，需要拆分 curr 和后面一个结点之间的连接关系，
            //在拆分之前需要存储 curr 的后一个结点 next；
            //令 curr 的 next 指针指向 null，完成 curr 和 next 的拆分；
            //将 next 赋值给 curr。
            ListNode next = cur.next;
            cur.next=null;
            cur=next;

        }
        return ret;
    }
}
//leetcode submit region end(Prohibit modification and deletion)
