class Solution {
    public int minArray(int[] numbers) {
        ////暴力解法
        //int min=numbers[0];
        //for (int i = 0; i < numbers.length; i++) {
        //    if(numbers[i]<=min){
        //        min = numbers[i];
        //    }
        //}
        //return min;
        //二分查找
        int l=0,h=numbers.length-1;
        while(l<h){
            int mid = l+(h-l)/2;
            if(numbers[mid]>numbers[h]){
                l=mid+1;
            }else if(numbers[mid]<numbers[h]){
                h=mid;
            }else{
                h--;
            }
        }
        return numbers[l];
    }
}

//runtime:0 ms
//memory:41.4 MB
