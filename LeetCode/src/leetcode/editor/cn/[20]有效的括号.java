//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串 s ，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 
//
// 
//
// 示例 1： 
//
// 
//输入：s = "()"
//输出：true
// 
//
// 示例 2： 
//
// 
//输入：s = "()[]{}"
//输出：true
// 
//
// 示例 3： 
//
// 
//输入：s = "(]"
//输出：false
// 
//
// 示例 4： 
//
// 
//输入：s = "([)]"
//输出：false
// 
//
// 示例 5： 
//
// 
//输入：s = "{[]}"
//输出：true 
//
// 
//
// 提示： 
//
// 
// 1 <= s.length <= 10⁴ 
// s 仅由括号 '()[]{}' 组成 
// 
// Related Topics 栈 字符串 👍 2972 👎 0


import java.util.Stack;

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean isValid(String s) {
        //使用栈
        Stack<Character> stack = new Stack<>();
        for (char c:
             s.toCharArray()) {
            //如果遇到左括号，就入栈
            if(c=='('||c=='{'||c=='['){
                stack.push(c);
            }else{//遇到右括号时
                if(stack.isEmpty()){
                    return false;
                }
                //弹出栈顶判断是否与该右括号匹配
                char c1 = stack.pop();
                boolean b1 = c==')'&&c1!='(';
                boolean b2 = c==']'&&c1!='[';
                boolean b3 = c=='}'&&c1!='{';
                if(b1||b2||b3){
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
//leetcode submit region end(Prohibit modification and deletion)
